import request from '@/utils/request'

export function getAllClassInfo() {
  return request({
    url: '/class/getAllClassInfo',
    method: 'get',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

// 添加班级
export function addClass(classInfo){
  return request({
    url: '/api/class/addClass',
    method: 'post',
    data: classInfo,
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

// 获取班级所有学生信息
export function getStudentsByClass(gradeId, classId) {
  return request({
    url: '/api/class/getStudentsByClass',
    method: 'get',
    params: {gradeId, classId},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getClassList(gradeId) {
  return request({
    url:'/api/class/getClassByGradeId',
    method:'get',
    params: {gradeId},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
