import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/api/user/login',
    method: 'post',
    data,
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}

export function getIdentity() {
  return request({
    url: '/api/user/getIdentity',
    method: 'post',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}
