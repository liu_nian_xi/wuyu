import request from '@/utils/request'

export function getDomainList() {
  return request({
    url: '/fuScale/getDomainList',
    method: 'get',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function insertFuScale(scaleInfo) {
  return request({
    url: '/fuScale/insertFuScale',
    method: 'post',
    data: scaleInfo,
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getFuScaleId() {
  return request({
    url: '/fuScale/getFuScaleId',
    method: 'get',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getFuScaleByTitle(title) {
  return request({
    url: '/fuScale/getFuScaleByTitle',
    method: 'get',
    params: {title: title},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getAllFuScale() {
  return request({
    url: '/fuScale/getAllFuScale',
    method: 'get',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getItemContent() {
  return request({
    url: '/fuScale/getItemContent',
    method: 'get',
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function deleteItemContent(itemId) {
  return request({
    url: '/fuScale/deleteItemContent',
    method: 'get',
    params: {itemId: itemId},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function insertScaleContent(scaleContent) {
  return request({
    url: '/fuScale/insertScaleContent',
    method: 'post',
    data: scaleContent,
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getContentById(scaleId) {
  return request({
    url: '/fuScale/getContentById',
    method: 'get',
    params: {scaleId: scaleId},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function getEditableContent(scaleId) {
  return request({
    url: '/fuScale/getEditableContent',
    method: 'get',
    params: {scaleId: scaleId},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}
