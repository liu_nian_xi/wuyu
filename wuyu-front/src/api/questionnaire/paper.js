import request from '@/utils/request1'

// 获取问卷信息列表
export function listPaper(query) {
  return request({
    url: '/questionnaire/paper/list',
    method: 'get',
    params: query,
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 查询问卷详细信息
export function getPaper(paperId) {
  return request({
    url: '/questionnaire/paper/' + paperId,
    method: 'get',
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 新增问卷信息
export function addPaper(data) {
  return request({
    url: '/questionnaire/paper',
    method: 'post',
    data: data,
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 修改问卷信息
export function updatePaper(data) {
  return request({
    url: '/questionnaire/paper',
    method: 'put',
    data: data,
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 删除问卷信息
export function delPaper(paperId) {
  return request({
    url: '/questionnaire/paper/' + paperId,
    method: 'delete',
    baseURL: 'http://82.157.231.88:9200'
  })
}
