import axios from 'axios'

export function fetchAllStudentList(params) {
  return axios({
    url: '/api/studentinfo/listAll',
    method: 'GET',
    params: params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function fetchStudentList(params) {
  return axios({
    url: '/api/studentinfo/list',
    method: 'GET',
    params: params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function fetchAllClassList(params) {
  return axios({
    url:'/api/classinfo/getclasslist',
    method:'GET',
    params:params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function deleteStudent(id) {
  return axios({
    url:'/api/studentinfo/delete/'+id,
    method:'get',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function createStudent(data) {
  return axios({
    url:'/api/studentinfo/add',
    method:'post',
    data:data,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function getStudent(id) {
  return axios({
    url:'/api/studentinfo/getstudent/'+id,
    method:'get',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function updateStudent(data) {
  console.log("====================================="+data.id)
  return axios({
    url:'/api/studentinfo/update',
    method:'post',
    params: data,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function updateScore(data) {
  console.log("#######################"+data.id)
  return axios({
    url:'/api/studentscore/updatescore',
    method:'post',
    params: data,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'

  })
}
export function addScore(params) {
  console.log("====================================="+params)
  return axios({
    url:'/api/studentscore/addscore',
    method:'post',
    params: params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
  export function getScore(id) {
    console.log("====================================="+id)
    return axios({
      url:'/api/studentscore/getscore/'+id,
      method:'get',
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'

    })
}
