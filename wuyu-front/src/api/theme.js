import request from '@/utils/request'

export function themeList(query) {
  return request({
    url: '/theme/find',
    method: 'get',
    params: query,
    // baseURL: 'http://82.157.231.88:9527'
    baseURL: 'http://localhost:9200'
  })
}

export function changeStatus(id, status) {
  return request({
    url: '/theme/updateStatus',
    method: 'post',
    params: { id, status },
    // baseURL: 'http://82.157.231.88:9527'
    baseURL: 'http://localhost:9200',
  })
}


export function createArticle(data) {
  return request({
    url: '/theme/insert',
    method: 'post',
    // baseURL: 'http://82.157.231.88:9527',
    baseURL: 'http://localhost:9200',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/theme/update',
    method: 'post',
    // baseURL: 'http://82.157.231.88:9527',
    baseURL: 'http://localhost:9200',
    data
  })
}

export function findTheme() {
  return request({
    url: '/theme/find',
    method: 'get',
    // baseURL: 'http://82.157.231.88:9527',
    baseURL: 'http://localhost:9200'
  })
}

export function deleteById(id) {
  return request({
    url: '/theme/delete',
    method: 'post',
    params: { id },
    // baseURL: 'http://82.157.231.88:9527',
    baseURL: 'http://localhost:9200'
  })
}
