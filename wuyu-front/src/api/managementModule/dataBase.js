import request from '@/utils/request';

// 获取拓扑图
export function getTopology() {
    return request({
      url: '/api/getTopology',
      method: 'get',
    })
}

// 获取基础信息
export function getPanelData() {
    return request({
      url: '/api/management/getPanelData',
      method: 'get',
    })
}