import request from '@/utils/request';

// 导出班级学生数据
export function getStudentsInClass(params) {
    let xhr = new XMLHttpRequest()
    let fileName = `${params.className}.xlsx` // 文件名称
    xhr.open('GET', `http://localhost:9200/api/class/downloadStudentInfo?classId=${params.classId}`, true)
    xhr.responseType = 'arraybuffer'
    xhr.onload = function() {
        if (this.status === 200) {
            let type = xhr.getResponseHeader('Content-Type')
            let blob = new Blob([this.response], {type: type})
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                window.navigator.msSaveBlob(blob, fileName)
            } else {
                let URL = window.URL || window.webkitURL
                let objectUrl = URL.createObjectURL(blob)
                if (fileName) {
                    var a = document.createElement('a')
                    if (typeof a.download === 'undefined') {
                    window.location = objectUrl
                    } else {
                    a.href = objectUrl
                    a.download = fileName
                    document.body.appendChild(a)
                    a.click()
                    a.remove()
                    }
                } else {
                    window.location = objectUrl
                }
            }
        }
    }
    xhr.send()
}

// 获取学生分页
export function getStudentsByPage(params) {
    return request({
      url: '/api/student/getStudentListByPage',
      method: 'get',
      params
    })
}

// 添加学生
export function addStudent(data){
    return request({
        url: '/api/student/addStudent',
        method: 'post',
        data
    })
}

// 修改学生
export function alterStudent(data){
    return request({
        url: '/api/student/alterStudent',
        method: 'put',
        data
    })
}

// 删除学生
export function deleteStudent(params){
    return request({
        url: '/api/student/deleteStudent',
        method: 'delete',
        params
    })
}

// 模版下载
export function getTemplate() {
    let xhr = new XMLHttpRequest()
    let fileName = `学生导入模版.xlsx` // 文件名称
    xhr.open('GET', `http://localhost:9200/api/student/getUploadTemplate`, true)
    xhr.responseType = 'arraybuffer'
    xhr.onload = function() {
        if (this.status === 200) {
            let type = xhr.getResponseHeader('Content-Type')
            let blob = new Blob([this.response], {type: type})
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                window.navigator.msSaveBlob(blob, fileName)
            } else {
                let URL = window.URL || window.webkitURL
                let objectUrl = URL.createObjectURL(blob)
                if (fileName) {
                    var a = document.createElement('a')
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                    window.location = objectUrl
                    } else {
                    a.href = objectUrl
                    a.download = fileName
                    document.body.appendChild(a)
                    a.click()
                    a.remove()
                    }
                } else {
                    window.location = objectUrl
                }
            }
        }
    }
    xhr.send()
}

// 导出全部学生
export function getStudentExcel(params) {
    let xhr = new XMLHttpRequest()
    let fileName = `学生汇总.xlsx` // 文件名称
    xhr.open('GET', `http://localhost:9200/api/student/download?keyword=${params.keyword ? params.keyword : ""}&gender=${params.gender ? params.gender : ''}&classId=${params.classId ? params.classId : ''}`, true)
    xhr.responseType = 'blob'
    xhr.onload = function() {
        if (this.status === 200) {
            let type = xhr.getResponseHeader('Content-Type')
            let blob = new Blob([this.response], {type: type})
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                window.navigator.msSaveBlob(blob, fileName)
            } else {
                let URL = window.URL || window.webkitURL
                let objectUrl = URL.createObjectURL(blob)
                if (fileName) {
                    var a = document.createElement('a')
                    if (typeof a.download === 'undefined') {
                    window.location = objectUrl
                    } else {
                    a.href = objectUrl
                    a.download = fileName
                    document.body.appendChild(a)
                    a.click()
                    a.remove()
                    }
                } else {
                    window.location = objectUrl
                }
            }
        }
    }
    xhr.send()
}
