import request from '@/utils/request';

// 班级对应Id接口 获取全部的班级信息
export function getAllClassInfo() {
    return request({
      url: '/api/class/getAllClassInfo',
      method: 'get',
      // baseURL: 'http://localhost:9200'
      baseURL: 'http://82.157.231.88:9200'
    })
}

// 全部班级分页
export function getClassesByPage(params) {
    return request({
      url: '/api/class/getClassesByPage',
      method: 'get',
      params
    })
}

// 添加班级信息
export function addClass(classInfo){
  return request({
    url: '/api/class/addClass',
    method: 'post',
    data: classInfo,
    // baseURL: 'http://localhost:9200'
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 添加班级负责老师信息
export function addTeacherToClass(correlationList){
  return request({
    url: '/api/class/addTeacherToClass',
    method: 'post',
    data: {correlationList},
    // baseURL: 'http://localhost:9200'
    baseURL: 'http://82.157.231.88:9200'
  })
}

// 编辑班级信息
export function editClass(data){
    return request({
        url: '/api/class/editClass',
        method: 'put',
        data
    })
}

// 删除班级
export function deleteClass(params){
    return request({
        url: '/api/class/deleteClass',
        method: 'delete',
        params
    })
}
