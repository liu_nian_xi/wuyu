import axios from 'axios'
export function fetchAllViewList(params) {
  return axios({
    url: '/api/view/listViewAll',
    method: 'GET',
    params: params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'

  })
}
export function deleteView(id) {
  return request({
    url:'/api/view/deleteview/'+id,
    method:'post',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function updateView(data) {
  console.log("====================================="+data.id)
  return axios({
    url:'/api/view/updateview',
    method:'post',
    params: data,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function getView(id) {
  return axios({
    url: '/api/view/getView/'+id,
    method: 'GET',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
export function fetchViewList(params) {
  return axios({
    url: '/api/view/listview',
    method: 'GET',
    params: params,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
