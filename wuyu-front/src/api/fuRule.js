import request from '@/utils/request'

export function insertRuleInfo(ruleForm) {
  return request({
    url: '/fuRule/insertRuleInfo',
    method: 'post',
    data: ruleForm,
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}

export function insertDetailList(isAllGrade, detailList) {
  return request({
    url: '/fuRule/insertDetailList',
    method: 'post',
    data: {isAllGrade: isAllGrade, detailList: detailList},
    baseURL: 'http://localhost:9200'
    // baseURL: 'http://82.157.231.88:9200'
  })
}
