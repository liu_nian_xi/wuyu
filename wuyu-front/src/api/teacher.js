import request from "@/utils/request";

export function createTeacher(data) {
    return request({
        url:'/teacher/create',
        method:'post',
        data:data,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'

    })
}

export function deleteTeacher(id) {
    return request({
        url:'/teacher/delete/'+id,
        method:'get',
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function getAllTeacher() {
  return request({
    url:'/teacher/getAllTeacher',
    method:'get',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getTeacherInfo(teacher_id) {
  return request({
    url:'/teacher/getTeacherInfo',
    method:'get',
    params: {teacher_id},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getTeacherByIdentity(identity) {
  return request({
    url:'/teacher/getTeacherByIdentity',
    method:'get',
    params: identity,
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getTeacherByClass(gradeId, classId) {
  return request({
    url:'/teacher/getTeacherByClass',
    method:'get',
    params: {gradeId, classId},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getMonitorByClass(gradeId, classId) {
  return request({
    url:'/teacher/getMonitorByClass',
    method:'get',
    params: {gradeId, classId},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getClassBasicInfo(teacher_id) {
  return request({
    url:'/teacher/getClassBasicInfo',
    method:'get',
    params: {teacher_id},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function updateTeacher(id,data) {
    return request({
        url:'/teacher/update/'+id,
        method:'post',
        data:data,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}
