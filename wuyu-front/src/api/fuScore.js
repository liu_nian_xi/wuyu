import request from "@/utils/request";

export default {
  //查询所有的班级
  getClass(current, limit, gradeNumber) {
    return request({
      url: `/api/class/findPage/${current}/${limit}/${gradeNumber}`,
      method: 'post',
    })
  }
}

export function getGradeFuScoreByDate(date) {
  return request({
    url:'/fuScore/getGradeFuScoreByDate',
    method:'get',
    params: {date},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getGradeScore(gradeId) {
  return request({
    url:'/fuScore/getGradeScore',
    method:'get',
    params: {gradeId},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}


export function getAllClassScore(gradeId) {
  return request({
    url:'/fuScore/getAllClassScore',
    method:'get',
    params: {gradeId},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getGradeOneScoreByDate(itemId, date) {
  return request({
    url:'/fuScore/getGradeOneScoreByDate',
    method:'get',
    params: {itemId, date},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getGradeFuScoreByItem(selectedItemID) {
  return request({
    url:'/fuScore/getGradeFuScoreByItem',
    method:'get',
    params: { selectedItemID },
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getGradeFuScoreByGradeID(gradeID, selectedItemID) {
  return request({
    url:'/fuScore/getGradeFuScoreByGradeID',
    method:'get',
    params: { gradeID, selectedItemID },
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getStudentsFuScoreList(gradeId, classId, date) {
  return request({
    url:'/fuScore/getStudentsFuScoreList',
    method:'get',
    params: { gradeId, classId, date },
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getStudentsFuScore(studentNum) {
  return request({
    url:'/fuScore/getStudentsFuScore',
    method:'get',
    params: { studentNum },
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getLastScore(studentNum, date) {
  return request({
    url: '/fuScore/getLastScore',
    method: 'get',
    params: {studentNum, date},
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}

export function getDateList() {
  return request({
    url:'/fuScore/getDateList',
    method:'get',
    // baseURL: 'http://82.157.231.88:9200'
    baseURL: 'http://localhost:9200'
  })
}
