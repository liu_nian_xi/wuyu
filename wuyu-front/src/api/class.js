
import axios from 'axios'
import request from "@/utils/request";

export default {
  //查询所有的班级
  getClass(current, limit, gradeNumber) {
    return request({
      url: `/api/class/findPage/${current}/${limit}/${gradeNumber}`,
      method: 'post',
    })
  }
}

export function fetchClassList(params) {
    return request({
        url:'/class/list',
        method:'get',
        params:params,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function fetchAllClassList(params) {
    return axios({
        url:'/api/classinfo/getclasslist',
        method:'GET',
        params:params,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function createClass(data) {
    return request({
        url:'/class/create',
        method:'post',
        data:data,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function deleteClass(id) {
    return request({
        url:'/class/delete/'+id,
        method:'get',
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function getClass(id) {
    return request({
        url:'/class/'+id,
        method:'get',
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

export function updateClass(id,data) {
    return request({
        url:'/class/update/'+id,
        method:'post',
        data:data,
      // baseURL: 'http://82.157.231.88:9200'
      baseURL: 'http://localhost:9200'
    })
}

