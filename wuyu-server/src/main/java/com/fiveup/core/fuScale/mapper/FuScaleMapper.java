package com.fiveup.core.fuScale.mapper;

import com.fiveup.core.fuScale.model.Domain;
import com.fiveup.core.fuScale.model.ScaleContent;
import com.fiveup.core.fuScale.model.ScaleInfo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FuScaleMapper {

    // 获取领域列表
    @Select("select * from fu_domain")
    List<Domain> getDomainList();

    // 新增评价量表表头
    @Insert("insert into fu_scale_content(title, create_date, state, creator_id, domain) " +
            "value(#{title}, #{createDate}, #{state}, #{creatorId}, #{domain})")
    int insertFuScale(ScaleInfo scaleInfo);

    // 新增评价内容
    @Insert("insert into fu_item_content(scale_id, item_num, item_content, pre_item, item_level, item_score, evaluation_method, evaluators, remark)" +
            "value(#{scaleId}, #{itemNum}, #{itemContent}, #{preItem}, #{itemLevel}, #{itemScore}, #{evaluationMethod}, #{evaluators}, #{remark})")
    int insertScaleContent(ScaleContent scaleContent);

    // 获取最新量表ID
    @Select("select max(scale_id) from fu_scale_content")
    int getFuScaleId();

    @Select("select scale_id from fu_scale_content where title=#{title}")
    int getFuScaleByTitle(String title);

    @Select("select * from fu_scale_content")
    List<ScaleInfo> getAllFuScale();

    // 获取最新量表ID
    @Select("select * from fu_item_content where item_id=(select max(item_id) from fu_item_content)")
    ScaleContent getItemContent();

    // 删除评价内容
    @Delete("delete from fu_item_content where item_id=#{itemId}")
    int deleteItemContent(int itemId);

    @Select("select * from fu_item_content where scale_id=#{scaleId}")
    List<ScaleContent> getContentById(Integer scaleId);

    @Select("select * from fu_item_content where scale_id=#{scaleId} and evaluators is not null")
    List<ScaleContent> getEditableContent(Integer scaleId);
}
