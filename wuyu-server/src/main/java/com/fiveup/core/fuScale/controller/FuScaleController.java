package com.fiveup.core.fuScale.controller;

import com.fiveup.core.fuScale.model.Domain;
import com.fiveup.core.fuScale.model.ScaleContent;
import com.fiveup.core.fuScale.model.ScaleInfo;
import com.fiveup.core.fuScale.service.FuScaleService;
import com.fiveup.core.management.common.CommonResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/fuScale")
public class FuScaleController {

    @Resource
    private FuScaleService fuScaleService;

    @GetMapping(value="/getDomainList")
    public CommonResponse getDomainList() {
        List<Domain> domainList = fuScaleService.getDomainList();
        return CommonResponse.ok(domainList);
    }

    @PostMapping(value="/insertFuScale")
    public CommonResponse insertFuScale(@RequestBody ScaleInfo scaleInfo) {
        int code = fuScaleService.insertFuScale(scaleInfo);
        return CommonResponse.ok();
    }

    @PostMapping(value="/insertScaleContent")
    public CommonResponse insertScaleContent(@RequestBody ScaleContent scaleContent) {
        System.out.println("insert item:");
        System.out.println(scaleContent);
        int code = fuScaleService.insertScaleContent(scaleContent);
        return CommonResponse.ok();
    }

    @GetMapping(value = "/getFuScaleId")
    public CommonResponse getFuScaleId() {
        int ScaleId ;
        ScaleId = fuScaleService.getFuScaleId();
        return CommonResponse.ok(ScaleId);
    }

    @GetMapping(value = "/getFuScaleByTitle")
    public CommonResponse getFuScaleByTitle(String title) {
        int ScaleId ;
        ScaleId = fuScaleService.getFuScaleByTitle(title);
        return CommonResponse.ok(ScaleId);
    }

    @GetMapping(value = "/getAllFuScale")
    public CommonResponse getAllFuScale() {
        List<ScaleInfo> scaleList;
        scaleList = fuScaleService.getAllFuScale();

        return CommonResponse.ok(scaleList);
    }

    @GetMapping(value = "/getItemContent")
    public CommonResponse getItemContent() {
        ScaleContent scaleContent;
        scaleContent = fuScaleService.getItemContent();
        return CommonResponse.ok(scaleContent);
    }

    @GetMapping(value = "/deleteItemContent")
    public CommonResponse deleteItemContent(int itemId) {
        System.out.println(itemId);
        int code = fuScaleService.deleteItemContent(itemId);
        return CommonResponse.ok(code);
    }

    @GetMapping(value = "/getContentById")
    public CommonResponse getContentById(@RequestParam Integer scaleId) {
        List<ScaleContent> scaleContentList;
        scaleContentList = fuScaleService.getContentById(scaleId);

        return CommonResponse.ok(scaleContentList);
    }

    @GetMapping(value = "/getEditableContent")
    public CommonResponse getEditableContent(@RequestParam Integer scaleId) {
        List<ScaleContent> scaleContentList;
        scaleContentList = fuScaleService.getEditableContent(scaleId);

        return CommonResponse.ok(scaleContentList);
    }
}
