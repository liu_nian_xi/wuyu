package com.fiveup.core.fuScale.service.impl;


import com.fiveup.core.fuScale.mapper.FuScaleMapper;
import com.fiveup.core.fuScale.model.Domain;
import com.fiveup.core.fuScale.model.ScaleContent;
import com.fiveup.core.fuScale.model.ScaleInfo;
import com.fiveup.core.fuScale.service.FuScaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: ParentServiceImpl
 * @Author: shilin
 * @Date: 2022/9/18 16:32
 */
@Service
public class FuScaleServiceImpl implements FuScaleService {

    @Autowired
    private FuScaleMapper fuScaleMapper;

    @Override
    public List<Domain> getDomainList() {
        List<Domain> domainList;
        domainList = fuScaleMapper.getDomainList();
        return domainList;
    }

    @Override
    public int insertFuScale(ScaleInfo scaleInfo) {
        return fuScaleMapper.insertFuScale(scaleInfo);
    }

    @Override
    public int insertScaleContent(ScaleContent scaleContent) {
        return fuScaleMapper.insertScaleContent(scaleContent);
    }

    @Override
    public int getFuScaleId() {
        return fuScaleMapper.getFuScaleId();
    }

    @Override
    public int getFuScaleByTitle(String title) {
        return fuScaleMapper.getFuScaleByTitle(title);
    }

    @Override
    public List<ScaleInfo> getAllFuScale() {
        return fuScaleMapper.getAllFuScale();
    }

    @Override
    public ScaleContent getItemContent() {
        return fuScaleMapper.getItemContent();
    }

    @Override
    public int deleteItemContent(int itemId) {
        return fuScaleMapper.deleteItemContent(itemId);
    }

    @Override
    public List<ScaleContent> getContentById(Integer scaleId) {
        return fuScaleMapper.getContentById(scaleId);
    }

    @Override
    public List<ScaleContent> getEditableContent(Integer scaleId) {
        return fuScaleMapper.getEditableContent(scaleId);
    }
}
