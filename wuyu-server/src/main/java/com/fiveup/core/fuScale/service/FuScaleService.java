package com.fiveup.core.fuScale.service;

import com.fiveup.core.fuScale.model.Domain;
import com.fiveup.core.fuScale.model.ScaleContent;
import com.fiveup.core.fuScale.model.ScaleInfo;

import java.util.List;

public interface FuScaleService {

    List<Domain> getDomainList();

    int insertFuScale(ScaleInfo scaleInfo);

    int insertScaleContent(ScaleContent scaleContent);

    int getFuScaleId();

    int getFuScaleByTitle(String title);

    List<ScaleInfo> getAllFuScale();

    ScaleContent getItemContent();

    int deleteItemContent(int itemId);

    List<ScaleContent> getContentById(Integer scaleId);

    List<ScaleContent> getEditableContent(Integer scaleId);
}
