package com.fiveup.core.fuScale.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

/**
 * @author shilin
 * @date 2022/9/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScaleInfo {
    private int scaleId;
    private String title;
    private String createDate;
    private int state;
    private int creatorId;
    private int domain;
}