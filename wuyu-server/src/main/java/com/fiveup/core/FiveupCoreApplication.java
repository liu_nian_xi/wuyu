package com.fiveup.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.mybatis.spring.annotation.MapperScan;

@EnableAspectJAutoProxy(proxyTargetClass=true)
@SpringBootApplication
@EnableScheduling
@MapperScan(basePackages = {"com.fiveup.core.management.mapper", "com.fiveup.core.events.mapper", "com.fiveup.core.demonstrate.mapper","com.fiveup.core.monitor.entity"})
public class FiveupCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(FiveupCoreApplication.class, args);
    }

}
