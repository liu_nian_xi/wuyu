package com.fiveup.core.events.mapper;

import com.fiveup.core.events.model.PictureActivity;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PictureActivityMapper {

    void insertOne(PictureActivity pictureActivity);

    @Select("  select picture_url\n" +
            "  from picture_activity\n" +
            "  where activity_id = #{activityId}")
    List<String> getUrlListByActivityId(Long activityId);


    List<String> getAllUrlList();
}
