package com.fiveup.core.miniapp.controller;

import com.fiveup.core.miniapp.model.UserMini;
import com.fiveup.core.miniapp.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fiveup.core.management.common.CommonResponse;


/**
 * @ClassName: ParentController
 * @Date: 2022/9/18 16:24
 * @author shilin
 */
@RestController
@RequestMapping("/miniapp")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @PostMapping (value = "/login")
    public CommonResponse login(@RequestBody UserMini userMini){

        UserMini response = userInfoService.login(userMini);
        if(response==null){
            return CommonResponse.fail(1001,"登录失败");
        }else{
            return CommonResponse.ok(response,"登录成功");
        }
    }
}
