package com.fiveup.core.miniapp.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.miniapp.model.TeacherMini;
import com.fiveup.core.miniapp.service.TchInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/miniapp")
public class TchInfoController {

    @Autowired
    private TchInfoService tchInfoService;

    @GetMapping("/getMonitorInfoByTchId")
    public CommonResponse getTchInfoByTchId(@RequestParam("teacherId") Long teacherId) {

        TeacherMini teacherMini;

        if(teacherId == null) {
            return CommonResponse.fail(1001, "服务器获取教师ID失败");
        } else {
            teacherMini = tchInfoService.getMonitorInfoByTchId(teacherId);
            return CommonResponse.ok(teacherMini);
        }
    }

    @GetMapping("/getMonitorInfoByTchName")
    public CommonResponse getTchInfoByTchName(@RequestParam("teacherName") String teacherName) {

        TeacherMini teacherMini;

        if(teacherName == "") {
            return CommonResponse.fail(1001, "服务器获取教师姓名失败");
        } else {
            teacherMini = tchInfoService.getMonitorInfoByTchName(teacherName);
            return CommonResponse.ok(teacherMini);
        }
    }
}
