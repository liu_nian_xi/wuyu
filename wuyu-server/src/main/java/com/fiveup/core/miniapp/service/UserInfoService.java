package com.fiveup.core.miniapp.service;

import com.fiveup.core.miniapp.model.UserMini;

public interface UserInfoService {

    int addParent(UserMini userMini);

    UserMini login(UserMini userMini);
}
