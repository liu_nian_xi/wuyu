package com.fiveup.core.miniapp.service;

import com.fiveup.core.management.model.DTO.StuDTO;

import java.util.List;

/**
 * @author shilin
 * @date 2022/9/19
 */
public interface StuInfoService {

    StuDTO getStudentInfo(Long studentNum);

    List<StuDTO> getAllStudent();

    List<StuDTO> getStuListByClassId(Long classId);
}
