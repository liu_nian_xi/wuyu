package com.fiveup.core.miniapp.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.model.DTO.StuDTO;
import com.fiveup.core.miniapp.service.ClassInfoService;
import com.fiveup.core.miniapp.service.StuInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author 史林
 * @date 2022/10/10
 * @function 学生相关信息查询--基本信息查询、体育得分查询
 */
@Slf4j
@RestController
@RequestMapping("/miniapp")
@CrossOrigin
public class StudentInfoController {


    @Resource
    private StuInfoService stuInfoService;

    @Autowired
    private ClassInfoService classInfoService;

    @PostMapping("/getStudentInfo")
    public CommonResponse<StuDTO> getStudentInfo(@RequestBody Map<String, Object> map) {
        String studentNum = (String) map.get("studentNum");
        if (studentNum == null) {
            return CommonResponse.fail(1001, "服务端获取学生学号失败");
        } else {
            StuDTO stuDTO = stuInfoService.getStudentInfo(Long.valueOf(studentNum));
            return CommonResponse.ok(stuDTO);
        }
    }

    @GetMapping("/findStudentByGradeAndClass")
    public CommonResponse<List<StuDTO>> getAllStudent(@RequestParam String grade,
                                                      @RequestParam String className) {
        System.out.println("grade :" + grade + "class" + className);
        List<StuDTO> studentList;
        if (grade == "" || className == "") {
            studentList = stuInfoService.getAllStudent();
        } else {
            Long classId = classInfoService.getIdByGradeAndClass(grade, className);
            studentList = stuInfoService.getStuListByClassId(classId);
        }
        return CommonResponse.ok(studentList);
    }

}
