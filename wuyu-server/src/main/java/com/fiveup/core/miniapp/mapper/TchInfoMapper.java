package com.fiveup.core.miniapp.mapper;

import com.fiveup.core.miniapp.model.TeacherMini;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TchInfoMapper {

    @Select("select * from basic_teacher where id=#{teacherId}")
    TeacherMini getTeacherById(Long teacherId);

    @Select("select * from basic_teacher where teacher_name=#{teacherName}")
    TeacherMini getTeacherByName(String teacherName);
}
