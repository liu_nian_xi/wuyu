package com.fiveup.core.miniapp.mapper;

import com.fiveup.core.miniapp.model.ClassMini;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ClassInfoMapper {

    @Select("select * from basic_class where id=#{classId}")
    ClassMini getClassInfoByClassId(Long classId);

    @Select("select id from basic_class where grade=#{grade} and class_name=#{className}")
    Long getIdByGradeAndClass(String grade, String className);

}
