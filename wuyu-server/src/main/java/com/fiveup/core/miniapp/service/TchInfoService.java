package com.fiveup.core.miniapp.service;

import com.fiveup.core.miniapp.model.TeacherMini;

/**
 * @author shilin
 * @date 2022/9/19
 */
public interface TchInfoService {

    TeacherMini getMonitorInfoByTchId(Long teacherId);

    TeacherMini getMonitorInfoByTchName(String teacherName);
}
