package com.fiveup.core.miniapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 史林
 * @date 2022/9/18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserMini {
    private Long id;
    private String username;
    private String password;
    private String realName;
    private String studentNum;
    private String familyRelationship;
    private Long studentClass;
    private Boolean role;
}
