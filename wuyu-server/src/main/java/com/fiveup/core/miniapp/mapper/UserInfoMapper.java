package com.fiveup.core.miniapp.mapper;

import com.fiveup.core.miniapp.model.UserMini;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserInfoMapper {

    @Insert("insert into basic_miniapp_user(username,password) values(#{username},#{password})")
    int addParent(UserMini userMini);

    @Select("select * from basic_miniapp_user where username=#{username} and password=#{password}")
    UserMini getParent(UserMini userMini);
}
