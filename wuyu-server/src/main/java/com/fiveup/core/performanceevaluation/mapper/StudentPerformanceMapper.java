package com.fiveup.core.performanceevaluation.mapper;

import com.fiveup.core.performanceevaluation.bean.StudentPerformance;
import com.fiveup.core.performanceevaluation.vo.Average;
import com.fiveup.core.performanceevaluation.vo.StudentPerformanceVO;
import jnr.ffi.annotations.In;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 对学生表现的数据操作
 */
public interface StudentPerformanceMapper {
    /**
     * 插入数据
     * @param studentPerformance
     */
    void insert(StudentPerformance studentPerformance);

    /**
     * 根据ID删除数据
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 根据ID更新数据
     * @param studentPerformance
     */
    void updateById(StudentPerformance studentPerformance);

    /**
     * 查询所有数据
     * @return
     */
    List<StudentPerformanceVO> selectAll();

    /**
     * 根据教师ID查询数据
     * @param id
     * @return
     */
    List<StudentPerformanceVO> selectByTId(Integer id);

    /**
     * 查询相关记录总数
     * @return
     */
    Integer selectCountBySql(String sql);

    /**
     * 分页查询
     * @param start
     * @param pageSize
     * @return
     */
    List<StudentPerformanceVO> selectPagination(Integer start, Integer pageSize);

    /**
     * 根据sid查询对应的数据
     * @param sid
     * @return
     */
    StudentPerformanceVO selectBySid(Integer sid);

    /**
     * 根据ID查询对应的数据
     * @param id
     * @return
     */
    StudentPerformanceVO selectById(Integer id);

    /**
     * 根据教师ID查询对应的平均分
     * @param tid
     * @return
     */
    Average selectAverageByTid(Integer tid);

    /**
     * 查询教师ID对应的除查询的学生之外的所有学生的总数
     * @param tid
     * @param sid
     * @return
     */
    List<Integer> selectSubjectScoreSum(Integer tid, Integer sid);
}
