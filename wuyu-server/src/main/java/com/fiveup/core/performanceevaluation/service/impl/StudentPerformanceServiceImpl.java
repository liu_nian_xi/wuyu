package com.fiveup.core.performanceevaluation.service.impl;

import com.fiveup.core.performanceevaluation.bean.StudentPerformance;
import com.fiveup.core.performanceevaluation.mapper.StudentPerformanceMapper;
import com.fiveup.core.performanceevaluation.mapper.SubjectScoreWeightMapper;
import com.fiveup.core.performanceevaluation.service.StudentPerformanceService;
import com.fiveup.core.performanceevaluation.utils.StudentPerformanceUtil;
import com.fiveup.core.performanceevaluation.vo.Average;
import com.fiveup.core.performanceevaluation.vo.StudentPerformanceVO;
import com.fiveup.core.performanceevaluation.vo.SubjectScoreWeightVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生表现业务逻辑实现
 */
@Service
public class StudentPerformanceServiceImpl implements StudentPerformanceService {

    @Autowired
    private StudentPerformanceMapper studentPerformanceMapper;

    @Override
    public void add(StudentPerformance studentPerformance) {
        studentPerformanceMapper.insert(studentPerformance);
    }

    @Override
    public void deleteById(Integer id) {
        studentPerformanceMapper.deleteById(id);
    }

    @Override
    public void updateById(StudentPerformance studentPerformance) {
        studentPerformanceMapper.updateById(studentPerformance);
    }

    @Override
    public List<StudentPerformanceVO> getAll() {
        List<StudentPerformanceVO> studentPerformanceVOS = studentPerformanceMapper.selectAll();
        return studentPerformanceVOS;
    }

    @Override
    public List<StudentPerformanceVO> getByTId(Integer tid) {
        List<StudentPerformanceVO> studentPerformanceVOS = studentPerformanceMapper.selectByTId(tid);
        return studentPerformanceVOS;
    }

    @Override
    public Integer getCount(String sql) {
        Integer count = studentPerformanceMapper.selectCountBySql(sql);
        return count;
    }

    @Override
    public List<StudentPerformanceVO> getPagination(Integer start, Integer pageSize) {
        List<StudentPerformanceVO> studentPerformanceVOS = studentPerformanceMapper.selectPagination(start, pageSize);
        return studentPerformanceVOS;
    }

    @Override
    public StudentPerformanceVO getBySid(Integer sid) {
        StudentPerformanceVO studentPerformanceVO = studentPerformanceMapper.selectBySid(sid);
        return studentPerformanceVO;
    }

    @Override
    public StudentPerformanceVO getById(Integer id) {
        StudentPerformanceVO studentPerformanceVO = studentPerformanceMapper.selectById(id);
        return studentPerformanceVO;
    }

    @Override
    public Average getAverageByTid(Integer tid) {
        Average average = studentPerformanceMapper.selectAverageByTid(tid);
        return average;
    }

    @Override
    public List<Integer> getSubjectScoreSum(Integer tid, Integer sid) {
        List<Integer> sums = studentPerformanceMapper.selectSubjectScoreSum(tid, sid);
        return sums;
    }
}
