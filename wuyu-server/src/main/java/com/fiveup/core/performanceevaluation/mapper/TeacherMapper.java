package com.fiveup.core.performanceevaluation.mapper;

import com.fiveup.core.performanceevaluation.bean.Teacher;

import java.util.List;

/**
 * 对教师的数据操作
 */
public interface TeacherMapper {
    /**
     * 根据教师ID查询数据
     * @param teacherID
     * @return
     */
    Teacher selectByTId(Integer teacherID);

    /**
     * 查询所有数据
     * @return
     */
    List<Teacher> selectAll();

    /**
     * 查询还未为课程设置权重的老师
     * @return
     */
    List<Teacher> selectNotInWeight(List<Integer> list);

    List<Teacher> selectInWeight(List<Integer> list);

    Teacher selectByName(String name);

    List<Integer> selectOtherTeacher(String name);
}
