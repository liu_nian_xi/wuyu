package com.fiveup.core.performanceevaluation.bean;

public class Teacher {
    // 主键ID
    private Integer teacherID;
    // 教师姓名
    private String username;
    // 账号密码
    private String password;

    public Teacher() {
    }

    public Teacher(Integer teacherID, String username, String password) {
        this.teacherID = teacherID;
        this.username = username;
        this.password = password;
    }

    public Integer getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(Integer teacherID) {
        this.teacherID = teacherID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
