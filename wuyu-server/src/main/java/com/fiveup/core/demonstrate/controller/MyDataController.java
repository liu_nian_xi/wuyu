package com.fiveup.core.demonstrate.controller;

import com.alibaba.fastjson.JSONObject;
import com.fiveup.core.collection.model.ColClass;
import com.fiveup.core.demonstrate.service.MyDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/demonstrate")
public class MyDataController {
    @Autowired
    private MyDataService myDataService;
    @RequestMapping(value = "/test")
    @ResponseBody
    public Map<String,Object> getList(){

        Map<String,Object> result=myDataService.test();
        result.put("code",200);
        return result;
    }

    @RequestMapping(value = "/getLineTj")
    @ResponseBody
    public Map<String,Object> getLineTj(){
        Map<String,Object> result=myDataService.getLineTj();
        result.put("code",200);
        return result;
    }

    //五育各年级成长
    @RequestMapping(value = "/getLineTj2")
    @ResponseBody
    public Map<String,Object> getLineTj2(){
        Map<String,Object> result=myDataService.getLineTj2();
        result.put("code",200);
        return result;
    }

    //全校完成度
    @RequestMapping(value = "/getWanchengdu")
    @ResponseBody
    public Map<String,Object> getWanchengdu(){
        Map<String,Object> result=myDataService.getWanchengdu();
        result.put("code",200);
        return result;
    }

    //年级成长（年级+各班）
    @RequestMapping(value = "/getTj")
    @ResponseBody
    public Map<String,Object> getTj(String type){
        Map<String,Object> result=myDataService.getTj(type);
        result.put("code",200);
        return result;
    }

    @RequestMapping(value = "/getGrade2")
    @ResponseBody
    public Map<String,Object> getGrade2(String type){
        Map<String,Object> result=myDataService.getGrade2(type);
        result.put("code",200);
        return result;
    }

    @RequestMapping(value = "/getGrade3")
    @ResponseBody
    public Map<String,Object> getGrade3(String type){
        Map<String,Object> result=myDataService.getGrade3(type);
        result.put("code",200);
        return result;
    }

    //全校整体五育数据
    @RequestMapping(value = "/getQxwycj")
    @ResponseBody
    public Map<String,Object> getQxwycj(String grade){
        Map<String,Object> result=myDataService.getQxwycj(grade);
        result.put("code",200);
        return result;
    }

    //    全校五育达成
    @RequestMapping(value = "/getWydc")
    @ResponseBody
    public Map<String,Object> getWydc(){
        Map<String,Object> result=myDataService.getWydc();
        result.put("code",200);
        return result;
    }

    //年级成长（各年级各班）
    @RequestMapping(value = "/getNjcz")
    @ResponseBody
    public Map<String,Object> getNjcz(){
        Map<String,Object> result=myDataService.getNjcz();
        result.put("code",200);
        return result;
    }

    //年级情况
    @RequestMapping(value = "/getNjqk")
    @ResponseBody
    public  Map<String,Object> getNjqk(){
        Map<String,Object> result=myDataService.getNjqk();
        result.put("code",200);
        return result;
    }

    @RequestMapping(value = "/getXYStudent")
    @ResponseBody
    public  Map<String,Object> getXYStudent(){
        Map<String,Object> result=myDataService.getXYStudent();
        result.put("code",200);
        return result;
    }

    @RequestMapping(value = "/getXYClass")
    @ResponseBody
    public  Map<String,Object> getXYClass(){
        Map<String,Object> result=myDataService.getXYClass();
        result.put("code",200);
        return result;
    }


    @RequestMapping(value = "/getSubLineTj")
    @ResponseBody
    public Map<String,Object> getSubLineTj(){
        Map<String,Object> result=myDataService.getSubLineTj();
        result.put("code",200);
        return result;
    }

    //详情展示数据
    @RequestMapping(value = "/getTable")
    @ResponseBody
    public Map<String,Object> getTable(@RequestBody String data){
        JSONObject object= JSONObject.parseObject(data);
        Map<String,Object> result= new HashMap<>();
        String queryParam = object.getString("queryParam");
        String range = object.getString("range");
        result=myDataService.getTable(queryParam,range);
        result.put("code",200);
        return result;
    }
}
