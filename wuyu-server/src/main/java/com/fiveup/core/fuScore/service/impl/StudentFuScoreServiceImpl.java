package com.fiveup.core.fuScore.service.impl;

import com.fiveup.core.fuScore.mapper.StudentFuScoreMapper;
import com.fiveup.core.fuScore.model.StudentFuScore;
import com.fiveup.core.fuScore.service.StudentFuScoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author shilin
 * @date 2022/9/19
 */
@Slf4j
@Service
public class StudentFuScoreServiceImpl implements StudentFuScoreService {


    @Resource
    private StudentFuScoreMapper studentFuScoreMapper;


    @Override
    public List<StudentFuScore> getStudentsFuScore(int studentId) {
        List<StudentFuScore> studentScore;
        studentScore = studentFuScoreMapper.getStudentsFuScore(studentId);
        return studentScore;
    }
}
