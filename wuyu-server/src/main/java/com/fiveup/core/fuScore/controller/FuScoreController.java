package com.fiveup.core.fuScore.controller;

import com.fiveup.core.classManage.service.ClassManageService;
import com.fiveup.core.fuScore.model.*;
import com.fiveup.core.fuScore.service.ClassFuScoreService;
import com.fiveup.core.fuScore.service.GradeFuScoreService;
import com.fiveup.core.fuScore.service.StudentFuScoreService;
import com.fiveup.core.management.common.CommonResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shilin
 * @date 2022/9/19
 */

@Slf4j
@RestController
@RequestMapping("/fuScore")
@CrossOrigin
public class FuScoreController {

    @Resource
    private StudentFuScoreService studentFuScoreService;
    @Resource
    private ClassFuScoreService classFuScoreService;
    @Resource
    private GradeFuScoreService gradeFuScoreService;
    @Resource
    private ClassManageService classManageService;

    // ItemID -- 项目名对应数组
    private String []fuItem = new String[]{"morality_score", "intelligence_score", "physical_score", "aesthetic_score", "labour_score"};

    // 获得某一班级所有学生的某一五育项目成绩
    @GetMapping("/getStudentsFuScoreList")
    public CommonResponse getStudentsFuScoreList( int gradeId, int classId, int date) {
        List<StudentFuScore> studentFuScore;
        studentFuScore = classFuScoreService.getStudentsFuScoreList(gradeId, classId, date);
        return CommonResponse.ok(studentFuScore);
    }

    // 获得所有年级的所有五育项目成绩
    @GetMapping("/getGradeFuScore")
    public CommonResponse<List<GradeFuScore>> getGradeFuScore() {
        List<GradeFuScore> gradeScoreList = null;
        gradeScoreList = gradeFuScoreService.getAll();
        return CommonResponse.ok(gradeScoreList);
    }

    // 根据时间搜索年级五育成绩
    @GetMapping("/getGradeFuScoreByDate")
    public CommonResponse<List<GradeFuScore>> getGradeFuScoreByDate(int date) {
        List<GradeFuScore> gradeScoreList;
        gradeScoreList = gradeFuScoreService.getGradeFuScoreByDate(date);
        System.out.println(gradeScoreList);
        return CommonResponse.ok(gradeScoreList);
    }

    // 搜索某一年级五育成绩
    @GetMapping("/getGradeScore")
    public CommonResponse<List<GradeFuScore>> getGradeScore(int gradeId) {
        List<GradeFuScore> gradeScoreList;
        gradeScoreList = gradeFuScoreService.getGradeScore(gradeId);
        System.out.println(gradeScoreList);
        return CommonResponse.ok(gradeScoreList);
    }

    // 获得所有年级的某时间下某一五育项目成绩
    @GetMapping("/getGradeOneScoreByDate")
    public CommonResponse<List<GradeFuItemScore>> getGradeOneScoreByDate(int itemId, int date) {
        List<GradeFuItemScore> gradeFuItemScore = null;
        if (String.valueOf(itemId).equals("")) {
            return CommonResponse.fail(1001, "服务端获取项目ID失败");
        } else {
            // 根据请求发送的ID判断需要哪一五育项目的成绩
            switch (itemId) {
                case 1: gradeFuItemScore = gradeFuScoreService.getGradeMoScoreByDate(date); break;
                case 2: gradeFuItemScore = gradeFuScoreService.getGradeInScoreByDate(date); break;
                case 3: gradeFuItemScore = gradeFuScoreService.getGradePhScoreByDate(date); break;
                case 4: gradeFuItemScore = gradeFuScoreService.getGradeAeScoreByDate(date); break;
                case 5: gradeFuItemScore = gradeFuScoreService.getGradeLaScoreByDate(date); break;
            }
            return CommonResponse.ok(gradeFuItemScore);
        }
    }

    // 获得所有年级的某一五育项目成绩
    @GetMapping("/getGradeFuScoreByItem")
    public CommonResponse<List<GradeFuItemScore>> getGradeFuScoreByItem(@RequestParam("selectedItemID") int itemID) {
        List<GradeFuItemScore> gradeFuItemScore = null;
        if (String.valueOf(itemID).equals("")) {
            return CommonResponse.fail(1001, "服务端获取班级ID失败");
        } else {
            // 根据请求发送的ID判断需要哪一五育项目的成绩
            switch (itemID) {
                case 1: gradeFuItemScore = gradeFuScoreService.getGradeMoScoreList(); break;
                case 2: gradeFuItemScore = gradeFuScoreService.getGradeInScoreList(); break;
                case 3: gradeFuItemScore = gradeFuScoreService.getGradePhScoreList(); break;
                case 4: gradeFuItemScore = gradeFuScoreService.getGradeAeScoreList(); break;
                case 5: gradeFuItemScore = gradeFuScoreService.getGradeLaScoreList(); break;
            }
            return CommonResponse.ok(gradeFuItemScore);
        }
    }

    // 获得某年级的某一五育项目成绩
    @GetMapping("/getGradeFuScoreByGradeID")
    public CommonResponse<List<ClassFuItemScore>> getGradeFuScoreByGradeID(@RequestParam("gradeID") int gradeID,
                                                                           @RequestParam("selectedItemID") int itemID) {
        List<ClassFuItemScore> classFuItemScore = null;
        if (String.valueOf(gradeID).equals("") || String.valueOf(itemID).equals("")) {
            return CommonResponse.fail(1001, "服务端获取数据失败");
        } else {
            // 根据请求发送的ID判断需要哪一五育项目的成绩
            switch (itemID) {
                case 1: classFuItemScore = gradeFuScoreService.getGradeMoScoreList(gradeID); break;
                case 2: classFuItemScore = gradeFuScoreService.getGradeInScoreList(gradeID); break;
                case 3: classFuItemScore = gradeFuScoreService.getGradePhScoreList(gradeID); break;
                case 4: classFuItemScore = gradeFuScoreService.getGradeAeScoreList(gradeID); break;
                case 5: classFuItemScore = gradeFuScoreService.getGradeLaScoreList(gradeID); break;
            }
            return CommonResponse.ok(classFuItemScore);
        }
    }

    // 获得某学生的所有时期五育项目成绩
    @GetMapping("/getStudentsFuScore")
    public CommonResponse getStudentsFuScore(int studentNum) {
        List<StudentFuScore> studentScore;
        studentScore = studentFuScoreService.getStudentsFuScore(studentNum);
        return CommonResponse.ok(studentScore);
    }

    // 获得成绩表中的评分时间
    @GetMapping("/getDateList")
    public CommonResponse<List<Integer>> getDateList() {
        List<Integer> dateList;
        dateList = gradeFuScoreService.getDateList();
        return CommonResponse.ok(dateList);
    }

    // 获得某学生去年同期成绩
    @GetMapping("/getLastScore")
    public CommonResponse getLastScore(String studentNum, int date) {
        StudentFuScore studentFuScore;
        studentFuScore = classFuScoreService.getLastScore(studentNum, date);
        return CommonResponse.ok(studentFuScore);
    }

    @GetMapping("/getAllClassScore")
    public CommonResponse getAllClassScore(int gradeId) {
        List<Integer> classList;
        List<List<ClassScore>> allClassScoreList = new ArrayList<>();
        classList = classManageService.getAllClassByGrade(gradeId);
        for(int i = 0; i < classList.size(); i++) {
            int classId = classList.get(i);
            System.out.println(classId);
            allClassScoreList.add(classFuScoreService.getClassScore(gradeId, classId));
        }
        return CommonResponse.ok(allClassScoreList);
    }
}
