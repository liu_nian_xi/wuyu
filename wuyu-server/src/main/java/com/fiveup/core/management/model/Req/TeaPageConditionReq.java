//package com.fiveup.core.management.model.Req;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
///**
// * @author 尔宣赫
// * @date 2022/4/15
// * *********************************DEPRECATED CLASS*********************************
// */
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//public class TeaPageConditionReq {
//    private String teacherName;
//    private String title;
//    private String position;
//    private Integer pageNum;
//    private Integer pageSize;
//}
