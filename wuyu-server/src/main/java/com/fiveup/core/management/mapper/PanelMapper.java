package com.fiveup.core.management.mapper;

/**
 * @author 尔宣赫
 * @date 2022/5/8
 */
public interface PanelMapper {

    Integer getStuCount(Long schoolId);
    Integer getTeacherCount(Long schoolId);
    Integer getClassCount(Long schoolId);
    Integer getGradeCount(Long schoolId);

}
