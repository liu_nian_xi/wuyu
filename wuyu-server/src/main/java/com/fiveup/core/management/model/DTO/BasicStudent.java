package com.fiveup.core.management.model.DTO;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("basic_student")
public class BasicStudent {
    private Integer id;
    //    private Long studentId;
    private String studentNum;
    private String studentName;
    private Integer gender;
    //    private String grade;
    private Long classId;
    //    private String className;
    //    private Integer scoreInclination;
    private String parentPhoneNum;
    //    private String schoolName;
    private Integer deleted;
}
