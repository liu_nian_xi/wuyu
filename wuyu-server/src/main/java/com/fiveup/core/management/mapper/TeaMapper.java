package com.fiveup.core.management.mapper;

import com.fiveup.core.management.model.DTO.TeaDTO;
import com.fiveup.core.management.model.Req.TeaAddReq;
import com.fiveup.core.management.model.Req.TeaEditReq;
import com.fiveup.core.management.model.Resp.TeaPageResp;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


import java.util.List;


@Mapper
public interface TeaMapper {

    @Select("select id,teacher_name,gender,phone_num,position,title,role " +
            "from basic_teacher " +
            "where id = #{teacherId} and deleted = 0")
    TeaDTO getTeacherById(@Param("teacherId") Long teacherId);

    @Select("select id, teacher_name " +
            "from basic_teacher " +
            "where deleted = 0 and school_id = #{schoolId}")
    List<TeaDTO> getTeacherSimpleInfo(@Param("schoolId") Long schoolId);

    List<TeaPageResp> getTeaByCondition(@Param("teacherName") String teacherName, @Param("title") String title, @Param("position") String position, @Param("schoolId")Long schoolId);

    void addOne(TeaAddReq teaAddReq);

    void edit(TeaEditReq teaEditReq);

    void softlyDeleteOne(Long teacherId);

}
