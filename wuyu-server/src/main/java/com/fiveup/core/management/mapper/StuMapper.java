package com.fiveup.core.management.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fiveup.core.management.model.DTO.BasicStudent;
import com.fiveup.core.management.model.DTO.StuDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface StuMapper extends BaseMapper<BasicStudent> {


    List<StuDTO> getStudentListByCondition(@Param("keyword") String keyword,
                                           @Param("gender") Integer gender,
                                           @Param("inclination") Integer inclination,
                                           @Param("classId") Long classId,
                                           @Param("schoolId") Long schoolId);


    void insertOne(StuDTO stuDTO);

    void updateOne(StuDTO stuDTO);

    @Select("select * from basic_student where student_num=#{studentNum}")
    StuDTO getStuInfoByStudentNum(Long studentNum);

    @Select("select * from basic_student")
    List<StuDTO> getAllStudent();

    List<StuDTO> getStuListByClassId(Long classId);

    void softlyDeleteById(@Param("studentId") String studentId);

    List<StuDTO> getStuListBySchoolIdAndGradeName(Long schoolId, String grade);
}
