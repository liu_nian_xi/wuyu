package com.fiveup.core.management.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.common.enums.BackendErrorCodeEnum;
import com.fiveup.core.management.service.StuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author 尔宣赫
 * @date 2022/4/18
 */
@Slf4j
@Controller
@CrossOrigin
public class UploadController {

    @Resource
    private StuService stuService;

    @GetMapping("/api/student/getUploadTemplate")
    public String downloadUploadStudentCSVTemplate() {
        return "redirect:/managementFile/upload-template.xlsx";
    }


    @PostMapping("/api/student/uploadStudents")
    @ResponseBody
    public CommonResponse<Void> uploadStudentExt(MultipartFile uploadFile) {
        try {
            log.info("uploadStudentExt start execute");
            stuService.uploadStuIntoDB(uploadFile);
            log.info("uploadStudentExt success");
            return CommonResponse.ok();
        } catch (Exception e) {
            log.error("uploadStudentExt_fail || error={}", e.getMessage(), e);
            return CommonResponse.fail(BackendErrorCodeEnum.UNSPECIFIED);
        }
    }
}
