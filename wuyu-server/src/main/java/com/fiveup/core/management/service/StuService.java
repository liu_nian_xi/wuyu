package com.fiveup.core.management.service;

import com.fiveup.core.management.model.DTO.StuDTO;
import com.fiveup.core.management.model.excel.StuDownloadExt;
import com.fiveup.core.management.model.excel.StuUploadExt;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author 尔宣赫
 * @date 2022/3/22
 */
public interface StuService {

    PageInfo<StuDTO> getStudentListByPage( String keyword, Integer gender, Integer inclination, Long classId,Long schoolId,Integer pageNum, Integer pageSize);

    List<StuDownloadExt> getStudentDownloadExtList(String keyword, Integer gender, Long classId,Long schoolId);

    void addStudent(StuDTO stuDTO);

    void alterStudent(StuDTO stuDTO);

    void deleteStudent(String studentId);

    void uploadStuIntoDB(MultipartFile file);
    /**
     * 用于将导入数据Batch-Size List保存到数据库中
     * @param stuUploadExtList
     */
    void save(List<StuUploadExt> stuUploadExtList);

    //按学号查询学生的数量
    long selectCount(String stunum);
}
