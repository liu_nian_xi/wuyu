package com.fiveup.core.management.service;

import com.fiveup.core.management.manage.entity.TaskInfo;
import com.fiveup.core.management.model.Req.BackupReq;

/**
 * @author 尔宣赫
 * @date 2022/5/1
 */
public interface BackupRecoverService {
    TaskInfo startBackup(BackupReq backupReq);
    TaskInfo recoverDB();

}
