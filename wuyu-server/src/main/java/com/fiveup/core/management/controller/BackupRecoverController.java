package com.fiveup.core.management.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.common.enums.BackendErrorCodeEnum;
import com.fiveup.core.management.manage.entity.TaskInfo;
import com.fiveup.core.management.model.Req.BackupReq;
import com.fiveup.core.management.service.BackupRecoverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 尔宣赫
 * @date 2022/5/1
 */
@Slf4j
@RestController
@CrossOrigin
public class BackupRecoverController {

    @Resource
    private BackupRecoverService backupRecoverService;

    @PostMapping("/api/startBackup")
    public CommonResponse<TaskInfo> backupStart(@RequestBody BackupReq backupReq) {
        try{
            // TODO 检验参数
            TaskInfo taskInfo = backupRecoverService.startBackup(backupReq);
            return CommonResponse.ok(taskInfo);
        }catch (Exception e) {
            return CommonResponse.fail(BackendErrorCodeEnum.UNSPECIFIED);
        }
    }


    @GetMapping("/api/recoverDataBase")
    public CommonResponse<TaskInfo> recoverLocalDataBase() {
        try{
            TaskInfo taskInfo = backupRecoverService.recoverDB();
            return CommonResponse.ok(taskInfo);
        }catch (Exception e) {
            return CommonResponse.fail(BackendErrorCodeEnum.UNSPECIFIED);
        }
    }

}
