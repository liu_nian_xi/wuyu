package com.fiveup.core.management.model.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author 尔宣赫
 * @date 2022/4/19
 */
@Data
public class StuUploadExt {

    @ExcelProperty("studentNum")
    private String studentNum;

    @ExcelProperty("studentName")
    private String studentName;

    @ExcelProperty("gender")
    private String genderChineseCharacter;

    @ExcelProperty("classId")
    private Long classId;

    @ExcelProperty("parentPhoneNum")
    private String parentPhoneNum;
}
