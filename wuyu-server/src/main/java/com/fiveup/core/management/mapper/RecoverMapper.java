package com.fiveup.core.management.mapper;

/**
 * @author 尔宣赫
 * @date 2022/5/2
 */
public interface RecoverMapper {

    void executeRecoverSQL(String sqlScriptName);
}
