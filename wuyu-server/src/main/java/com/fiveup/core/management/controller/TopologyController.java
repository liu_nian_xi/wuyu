package com.fiveup.core.management.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.common.enums.BackendErrorCodeEnum;
import com.fiveup.core.management.model.topology.TopologyNode;
import com.fiveup.core.management.service.CommonManagementService;
import com.fiveup.core.management.service.TopologyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 尔宣赫
 * @date 2022/4/25
 */
@Slf4j
@RestController
@CrossOrigin
public class TopologyController {


    @Resource
    private CommonManagementService commonManagementService;

    @Resource
    private TopologyService topologyService;

    @GetMapping("/api/getTopology")
    public CommonResponse<TopologyNode> getTopology() {
        Long schoolId = commonManagementService.getSchoolId();
        try {
            TopologyNode topologyNode = topologyService.getTopology(schoolId);
            return CommonResponse.ok(topologyNode);
        }catch (Exception e) {
            log.error("getTopology_fail || error={}",e.getMessage(),e);
            return CommonResponse.fail(BackendErrorCodeEnum.UNSPECIFIED);
        }

    }
}
