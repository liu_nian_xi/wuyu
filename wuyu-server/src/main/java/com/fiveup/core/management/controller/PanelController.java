package com.fiveup.core.management.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.model.Resp.Panel;
import com.fiveup.core.management.model.Resp.PanelStatisticResp;
import com.fiveup.core.management.model.topology.TopologyNode;
import com.fiveup.core.management.service.CommonManagementService;
import com.fiveup.core.management.service.PanelService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 尔宣赫
 * @date 2022/4/25
 */
@Slf4j
@RestController
@CrossOrigin
public class PanelController {

    @Resource
    private CommonManagementService commonManagementService;

    @Resource
    private PanelService panelService;

    @GetMapping("/api/management/getPanelData")
    public CommonResponse<PanelStatisticResp> getPanelData() {
        Long schoolId = commonManagementService.getSchoolId();


        PanelStatisticResp panelStatisticResp = panelService.getStatisticData(schoolId);
        // MOCK
//        PanelStatisticResp panelStatisticResp = new PanelStatisticResp(new Panel(60,12,6,6));

        
        return CommonResponse.ok(panelStatisticResp);
    }




}
