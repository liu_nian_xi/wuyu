package com.fiveup.core.management.service.impl;

import com.fiveup.core.management.service.CommonManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 尔宣赫
 * @date 2022/4/25
 */

@Slf4j
@Service
public class CommonManagementServiceImpl implements CommonManagementService {


    @Override
    public Long getSchoolId() {

        // TODO 与负责用户管理的同学协调，完成对用户所属学校的识别

        // 1 从cookie或session或redis中获取用户信息

        // 2 从用户信息中提取出用户所属学校Id

        // 3 返回用户所属学校Id

        return 1L;
    }


}
