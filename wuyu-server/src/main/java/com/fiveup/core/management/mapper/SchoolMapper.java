package com.fiveup.core.management.mapper;

import com.fiveup.core.management.model.School;

/**
 * @author 尔宣赫
 * @date 2022/4/25
 */
public interface SchoolMapper {
    School getSchoolById(Long schoolId);
}
