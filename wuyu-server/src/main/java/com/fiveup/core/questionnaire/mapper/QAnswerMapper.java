package com.fiveup.core.questionnaire.mapper;

import java.util.List;
import com.fiveup.core.questionnaire.domain.QAnswer;

/**
 * 答题管理Mapper接口
 * 
 * @author admin
 * @date
 */
public interface QAnswerMapper 
{
    /**
     * 查询答题管理
     * 
     * @param answerId 答题管理主键
     * @return 答题管理
     */
    public QAnswer selectQAnswerByAnswerId(Long answerId);

    /**
     * 查询答题管理列表
     * 
     * @param qAnswer 答题管理
     * @return 答题管理集合
     */
    public List<QAnswer> selectQAnswerList(QAnswer qAnswer);

    /**
     * 新增答题管理
     * 
     * @param qAnswer 答题管理
     * @return 结果
     */
    public int insertQAnswer(QAnswer qAnswer);

    /**
     * 修改答题管理
     * 
     * @param qAnswer 答题管理
     * @return 结果
     */
    public int updateQAnswer(QAnswer qAnswer);

    /**
     * 删除答题管理
     * 
     * @param answerId 答题管理主键
     * @return 结果
     */
    public int deleteQAnswerByAnswerId(Long answerId);

    /**
     * 批量删除答题管理
     * 
     * @param answerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteQAnswerByAnswerIds(Long[] answerIds);
}
