package com.fiveup.core.questionnaire.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.management.model.DTO.TeaDTO;
import com.fiveup.core.questionnaire.dto.Identity;
import com.fiveup.core.questionnaire.dto.User;
import com.fiveup.core.questionnaire.service.UserService;
import com.fiveup.core.questionnaire.vo.ResponseVO;
import com.fiveup.core.questionnaire.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName: UserController
 * @Date: 2021/4/26 13:42
 * @author zhaomin
 */
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseVO addUser(@RequestBody UserVO userVO){
        return userService.addUser(userVO);
    }

    //教师用户登陆
    @PostMapping(value = "/login")
    public CommonResponse login(@RequestBody User user){
        User response = userService.login(user);
        if(response==null){
            return CommonResponse.fail(10001,"登录失败，请检查登录信息！");
        }else{
            return CommonResponse.ok(response);
        }
    }

    //查询所有系统角色
    @PostMapping(value = "/getIdentity")
    public CommonResponse getIdentity(){
        System.out.println("...");
        List<Identity> identityList = userService.getIdentity();
        if(identityList==null){
            return CommonResponse.fail(10001,"查询失败");
        }else{
            return CommonResponse.ok(identityList);
        }
    }
}

