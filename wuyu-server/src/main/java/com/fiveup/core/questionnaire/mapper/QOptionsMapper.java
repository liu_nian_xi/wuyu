package com.fiveup.core.questionnaire.mapper;

import java.util.List;
import com.fiveup.core.questionnaire.domain.QOptions;

/**
 * 选项管理Mapper接口
 * 
 * @author admin
 * @date
 */
public interface QOptionsMapper 
{
    /**
     * 查询选项管理
     * 
     * @param optId 选项管理主键
     * @return 选项管理
     */
    public QOptions selectQOptionsByOptId(Long optId);

    /**
     * 查询选项管理列表
     * 
     * @param qOptions 选项管理
     * @return 选项管理集合
     */
    public List<QOptions> selectQOptionsList(QOptions qOptions);

    /**
     * 新增选项管理
     * 
     * @param qOptions 选项管理
     * @return 结果
     */
    public int insertQOptions(QOptions qOptions);

    /**
     * 修改选项管理
     * 
     * @param qOptions 选项管理
     * @return 结果
     */
    public int updateQOptions(QOptions qOptions);

    /**
     * 删除选项管理
     * 
     * @param optId 选项管理主键
     * @return 结果
     */
    public int deleteQOptionsByOptId(Long optId);

    /**
     * 批量删除选项管理
     * 
     * @param optIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteQOptionsByOptIds(Long[] optIds);
}
