package com.fiveup.core.questionnaire.service.impl;

import java.util.List;

import com.fiveup.core.questionnaire.mapper.QQuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fiveup.core.questionnaire.mapper.QPaperMapper;
import com.fiveup.core.questionnaire.domain.QPaper;
import com.fiveup.core.questionnaire.service.IQPaperService;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 问卷信息Service业务层处理
 * 
 * @author
 * @date
 */
@Service
public class QPaperServiceImpl implements IQPaperService 
{
    @Autowired
    private QPaperMapper qPaperMapper;
    @Autowired
    private QQuestionMapper qQuestionMapper;

    /**
     * 查询问卷信息
     * 
     * @param paperId 问卷信息主键
     * @return 问卷信息
     */
    @Override
    public QPaper selectQPaperByPaperId(Long paperId)
    {
        return qPaperMapper.selectQPaperByPaperId(paperId);
    }

    /**
     * 查询问卷信息列表
     * 
     * @param qPaper 问卷信息
     * @return 问卷信息
     */
    @Override
    public List<QPaper> selectQPaperList(QPaper qPaper)
    {
        return qPaperMapper.selectQPaperList(qPaper);
    }

    /**
     * 新增问卷信息
     * 
     * @param qPaper 问卷信息
     * @return 结果
     */
    @Override
    @Transactional(propagation=Propagation.NESTED,isolation= Isolation.READ_COMMITTED)
    public int insertQPaper(QPaper qPaper)
    {
        int i = qPaperMapper.insertQPaper(qPaper);
        return i;
    }

    /**
     * 修改问卷信息
     * 
     * @param qPaper 问卷信息
     * @return 结果
     */
    @Override
    public int updateQPaper(QPaper qPaper) {
        int i = qQuestionMapper.selcetListByPaperId(qPaper.getPaperId());
        String paperStatus = qPaperMapper.selectQPaperByPaperId(qPaper.getPaperId()).getPaperStatus();
        if (("0".equals(paperStatus)&&i<=0&qPaper.getPaperStatus().equals("1"))||(paperStatus==qPaper.getPaperStatus()&&"1".equals(paperStatus))){
            return  -1;
         }else{
            return qPaperMapper.updateQPaper(qPaper);
      }
    }

    /**
     * 批量删除问卷信息
     * 
     * @param paperIds 需要删除的问卷信息主键
     * @return 结果
     */
    @Override
    public int deleteQPaperByPaperIds(Long[] paperIds)
    {
        return qPaperMapper.deleteQPaperByPaperIds(paperIds);
    }

    /**
     * 删除问卷信息信息
     * 
     * @param paperId 问卷信息主键
     * @return 结果
     */
    @Override
    public int deleteQPaperByPaperId(Long paperId)
    {
        return qPaperMapper.deleteQPaperByPaperId(paperId);
    }
}
