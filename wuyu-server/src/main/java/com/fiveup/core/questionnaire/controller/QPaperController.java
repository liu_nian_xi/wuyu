package com.fiveup.core.questionnaire.controller;

import java.util.List;

import com.fiveup.core.common.result.ResultBo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fiveup.core.questionnaire.domain.QPaper;
import com.fiveup.core.questionnaire.service.IQPaperService;

/**
 * 问卷信息Controller
 *
 * @author
 * @date
 */
@RestController
@RequestMapping("questionnaire/paper")
public class QPaperController {
    @Autowired
    private IQPaperService qPaperService;

    /**
     * 获取整个问卷列表
     */
    @GetMapping("/list")
    public ResultBo list(QPaper qPaper) {
        PageHelper.startPage(qPaper.getPageNum(), qPaper.getPageSize());
        List<QPaper> list = qPaperService.selectQPaperList(qPaper);
        PageInfo<QPaper> pageInfo = new PageInfo<>(list);
        return ResultBo.ok(pageInfo.getList(), pageInfo.getTotal());
    }


    @GetMapping(value = "/{paperId}")
    public ResultBo getInfo(@PathVariable("paperId") Long paperId) {
        return ResultBo.ok(qPaperService.selectQPaperByPaperId(paperId));
    }

    /**
     * 新增问卷基本信息
     */
    @PostMapping
    public ResultBo add(@RequestBody QPaper qPaper) {
        return ResultBo.ok(qPaperService.insertQPaper(qPaper));
    }

    /**
     * 修改问卷基本信息
     */
    @PutMapping
    public ResultBo edit(@RequestBody QPaper qPaper) {
        int data = qPaperService.updateQPaper(qPaper);
        if(data<0&&"1".equals(qPaper.getPaperStatus())){
            return ResultBo.error(403,"没有题目不能发布");
        }
        return ResultBo.ok(data);
    }

    /**
     * 删除对应ID问卷
     */
    @DeleteMapping("/{paperIds}")
    public ResultBo remove(@PathVariable Long[] paperIds) {
        return ResultBo.ok(qPaperService.deleteQPaperByPaperIds(paperIds));
    }
}
