package com.fiveup.core.questionnaire.controller;

import java.util.List;

import com.fiveup.core.common.result.ResultBo;
import com.fiveup.core.questionnaire.dto.QuestionDto;
import com.fiveup.core.questionnaire.enums.PaperStatus;
import com.fiveup.core.questionnaire.mapper.PaperMapper;
import com.fiveup.core.questionnaire.vo.PaperQuestionVo;
import com.fiveup.core.questionnaire.vo.PaperVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fiveup.core.questionnaire.domain.QQuestion;
import com.fiveup.core.questionnaire.service.IQQuestionService;

/**
 * 题目管理Controller
 * 
 * @author admin
 * @date
 */
@RestController
@RequestMapping("/questionnaire/question")
public class QQuestionController
{
    @Autowired
    private IQQuestionService qQuestionService;
    @Autowired
    private PaperMapper paperMapper;


    /**
     * 预览问卷题目
     */
    @GetMapping("/list")
    public ResultBo list(QQuestion qQuestion)
    {
//           System.out.println("查询"+qQuestion);
           List<QQuestion> list = qQuestionService.selectQQuestionList(qQuestion);
           return ResultBo.ok(list, Long.parseLong(list.size()+""));

    }

    /**
     * 数据统计查询
     */
    @GetMapping("/listCount")
    public ResultBo AnswerCount(QQuestion qQuestion)
    {
//        System.out.println("统计"+qQuestion);
        List<QQuestion> list = qQuestionService.selectAnswerCount(qQuestion);
        return ResultBo.ok(list, Long.parseLong(list.size()+""));
    }

    /**
     * 获取题目管理详细信息
     */
    @GetMapping(value = "/{questId}")
    public ResultBo getInfo(@PathVariable("questId") Long questId)
    {
        System.out.println("查询题目"+questId);
        return ResultBo.ok(qQuestionService.selectQQuestionByQuestId(questId));
    }

    /**
     * 新增题目
     */
    @PostMapping
    public ResultBo add(@RequestBody QQuestion qQuestion)
    {
//        System.out.println("添加题目"+qQuestion);
        return ResultBo.ok(qQuestionService.insertQQuestion(qQuestion));
    }

    /**
     * 修改题目管理
     */
    @PutMapping
    public ResultBo edit(@RequestBody QQuestion qQuestion)
    {
        System.out.println("修改题目"+qQuestion);
        return ResultBo.ok(qQuestionService.updateQQuestion(qQuestion));
    }

    /**
     * 删除题目管理
     */
	@DeleteMapping("/{questIds}")
    public ResultBo remove(@PathVariable Long[] questIds)
    {
        System.out.println("删除"+questIds);
        return ResultBo.ok(qQuestionService.deleteQQuestionByQuestIds(questIds));
    }

    //修改题目选项
    @PostMapping("/updQuestion")
    public  ResultBo updQuestion(@RequestBody QuestionDto questionDto){
//        System.out.println("++++++++++++++==="+questionDto);
        return  ResultBo.ok(qQuestionService.updQuestion(questionDto));
    }

    //删除题目选项
    @DeleteMapping("/delQuest")
    public  ResultBo delQuest(@RequestBody QuestionDto questionDto){
//        System.out.println("------------==="+questionDto);
	    return ResultBo.ok(qQuestionService.delquest(questionDto));
    }

    //链接或二维码填写问卷
    @GetMapping("/list2")
    public ResultBo list2(QQuestion qQuestion)
    {
//        System.out.println("================="+qQuestion.getPaperId().toString());
        PaperVO paperVO = paperMapper.selectByPaperId(Integer.valueOf(qQuestion.getPaperId().toString()));
        if(!paperVO.getPaperStatus().equals(PaperStatus.STOP.getCode())&&!paperVO.getPaperStatus().equals(PaperStatus.INIT.getCode())) {
            List<QQuestion> list = qQuestionService.selectQQuestionList(qQuestion);
            PaperVO paperVO1 = paperMapper.selectByPaperId(Integer.parseInt(qQuestion.getPaperId().toString()));
            PaperQuestionVo paperQuestionVo=new PaperQuestionVo();
            paperQuestionVo.setqQuestions(list);
            paperQuestionVo.setPaperVO(paperVO1);
            return ResultBo.ok(paperQuestionVo,Long.parseLong(list.size()+"") );
        }else{
            return ResultBo.error(103,"问卷已经截止或未发布");
        }

    }
}
