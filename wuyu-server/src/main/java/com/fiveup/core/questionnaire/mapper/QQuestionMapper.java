package com.fiveup.core.questionnaire.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fiveup.core.questionnaire.domain.QQuestion;

/**
 * 题目管理Mapper接口
 * 
 * @author admin
 * @date
 */
public interface QQuestionMapper
{
    /**
     * 查询题目管理
     * 
     * @param questId 题目管理主键
     * @return 题目管理
     */
    public QQuestion selectQQuestionByQuestId(Long questId);

    /**
     * 查询题目管理列表
     * 
     * @param qQuestion 题目管理
     * @return 题目管理集合
     */
    public List<QQuestion> selectQQuestionList(QQuestion qQuestion);

    /**
     * 新增题目管理
     * 
     * @param qQuestion 题目管理
     * @return 结果
     */
    public int insertQQuestion(QQuestion qQuestion);

    /**
     * 修改题目管理
     * 
     * @param qQuestion 题目管理
     * @return 结果
     */
    public int updateQQuestion(QQuestion qQuestion);

    /**
     * 删除题目管理
     * 
     * @param questId 题目管理主键
     * @return 结果
     */
    public int deleteQQuestionByQuestId(Long questId);

    /**
     * 批量删除题目管理
     * 
     * @param questIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteQQuestionByQuestIds(Long[] questIds);

    List<QQuestion> selectAnswerCount(QQuestion qQuestion);

    /**
     * 删除题目
     */
    public  int delQuestoptList(Long questId);

   int selcetListByPaperId(Long id);
}
