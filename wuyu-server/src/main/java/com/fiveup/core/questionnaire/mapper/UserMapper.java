package com.fiveup.core.questionnaire.mapper;

import com.fiveup.core.questionnaire.dto.Identity;
import com.fiveup.core.questionnaire.dto.User;
import com.fiveup.core.questionnaire.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface UserMapper {

    int addUser(UserVO userVO);

    @Select("select username, password, id from basic_web_user where username=#{username} and password=#{password}")
    User getUserByID(User user);

    @Select("select identity_id, identity_info from basic_user_identity")
    List<Identity> getIdentity();
}
