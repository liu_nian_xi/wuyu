package com.fiveup.core.teacherworkspace.mapper;

import com.fiveup.core.teacherworkspace.model.ClassBasicInfo;
import com.fiveup.core.teacherworkspace.model.LessonTeacher;
import com.fiveup.core.teacherworkspace.model.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TeacherWorkspaceMapper {

    @Select("select user_id, identity, real_name from basic_user where identity>=3 and identity<=8")
    List<Teacher> getAllTeacher();

    @Select("select id, teacher_name, gender, phone_num, position, title, role from basic_teacher where id=#{teacherId}")
    Teacher getTeacherInfo(Long teacher_id);

    // 多表查询，结果包括班级ID、年级、班级、学生人数
    @Select("select basic_class.id, grade, class_name, COUNT(student_name) as student_count " +
            "from basic_class, basic_student " +
            "where monitor_id=#{teacherId} and basic_student.class_id = basic_class.id")
    ClassBasicInfo getClassBasicInfo(Long teacher_id);

    @Select("select real_name from basic_user where identity=3")
    List<String> getTeacherByIdentity();

    @Select("select real_name, gender, identity_info " +
            "from correlation_grade_teacher a, basic_user b, basic_user_identity c " +
            "where grade_id=#{gradeId} and class_id=#{classId} and a.teacher_id=b.user_id and b.identity=c.identity_id")
    List<LessonTeacher> getTeacherByClass(int gradeId, int classId);

    @Select("SELECT real_name, contact_info " +
            "FROM basic_user join correlation_grade_teacher on user_id = teacher_id " +
            "where grade_id=2 and class_id=3 and identity=3")
    LessonTeacher getMonitorByClass(int gradeId, int classId);
}
