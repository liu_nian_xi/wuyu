package com.fiveup.core.teacherworkspace.service.impl;


import com.fiveup.core.miniapp.mapper.ParentMapper;
import com.fiveup.core.teacherworkspace.mapper.TeacherWorkspaceMapper;
import com.fiveup.core.teacherworkspace.model.ClassBasicInfo;
import com.fiveup.core.teacherworkspace.model.LessonTeacher;
import com.fiveup.core.teacherworkspace.model.Teacher;
import com.fiveup.core.teacherworkspace.service.TeacherWorkspaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: ParentServiceImpl
 * @Author: shilin
 * @Date: 2022/9/18 16:32
 */
@Service
public class TeacherWorkspaceServiceImpl implements TeacherWorkspaceService {

    @Autowired
    TeacherWorkspaceMapper teacherWorkspaceMapper;

    @Override
    public List<Teacher> getAllTeacher() {
        return teacherWorkspaceMapper.getAllTeacher();
    }

    @Override
    public Teacher getTeacherInfo(Long teacher_id) {
        return teacherWorkspaceMapper.getTeacherInfo(teacher_id);
    }

    @Override
    public ClassBasicInfo getClassBasicInfo(Long teacher_id) {
        return teacherWorkspaceMapper.getClassBasicInfo(teacher_id);
    }

    @Override
    public List<String> getTeacherByIdentity() {
        List<String> teacherList;
        teacherList = teacherWorkspaceMapper.getTeacherByIdentity();
        return teacherList;
    }

    @Override
    public List<LessonTeacher> getTeacherByClass(int gradeId, int classId) {
        List<LessonTeacher> lessonTeacherList;
        lessonTeacherList = teacherWorkspaceMapper.getTeacherByClass(gradeId, classId);
        return lessonTeacherList;
    }

    @Override
    public LessonTeacher getMonitorByClass(int gradeId, int classId) {
        LessonTeacher lessonTeacherList;
        lessonTeacherList = teacherWorkspaceMapper.getMonitorByClass(gradeId, classId);
        return lessonTeacherList;
    }
}
