package com.fiveup.core.teacherworkspace.controller;

import com.fiveup.core.management.common.CommonResponse;
import com.fiveup.core.teacherworkspace.model.ClassBasicInfo;
import com.fiveup.core.teacherworkspace.model.LessonTeacher;
import com.fiveup.core.teacherworkspace.model.Teacher;
import org.springframework.web.bind.annotation.*;
import com.fiveup.core.teacherworkspace.service.TeacherWorkspaceService;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/teacher")
public class TeacherWorkspaceController {

    @Resource
    private TeacherWorkspaceService teacherWorkspaceService;

    @GetMapping(value="/getAllTeacher")
    public CommonResponse getAllTeacher() {
        List<Teacher> teacherList = teacherWorkspaceService.getAllTeacher();

        return CommonResponse.ok(teacherList);
    }

    @GetMapping(value="/getTeacherInfo")
    public CommonResponse getTeacherInfo(@RequestParam Long teacher_id) {

        Teacher teacher = teacherWorkspaceService.getTeacherInfo(teacher_id);

        return CommonResponse.ok(teacher);
    }

    @GetMapping(value="/getTeacherByIdentity")
    public CommonResponse getTeacherByIdentity() {

        List<String> teacherList;
        teacherList = teacherWorkspaceService.getTeacherByIdentity();

        return CommonResponse.ok(teacherList);
    }

    @GetMapping(value="/getTeacherByClass")
    public CommonResponse getTeacherByClass(int gradeId, int classId) {

        List<LessonTeacher> lessonTeacherList;
        lessonTeacherList = teacherWorkspaceService.getTeacherByClass(gradeId, classId);

        return CommonResponse.ok(lessonTeacherList);
    }

    @GetMapping(value="/getMonitorByClass")
    public CommonResponse getMonitorByClass(int gradeId, int classId) {

        LessonTeacher lessonTeacherList;
        lessonTeacherList = teacherWorkspaceService.getMonitorByClass(gradeId, classId);

        return CommonResponse.ok(lessonTeacherList);
    }

    @GetMapping(value="/getClassBasicInfo")
    public CommonResponse getClassBasicInfo(@RequestParam Long teacher_id) {
        System.out.println(teacher_id);

        ClassBasicInfo classBasicInfo = teacherWorkspaceService.getClassBasicInfo(teacher_id);

        return CommonResponse.ok(classBasicInfo);
    }
}
