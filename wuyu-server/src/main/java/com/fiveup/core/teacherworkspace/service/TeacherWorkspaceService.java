package com.fiveup.core.teacherworkspace.service;

import com.fiveup.core.teacherworkspace.model.ClassBasicInfo;
import com.fiveup.core.teacherworkspace.model.LessonTeacher;
import com.fiveup.core.teacherworkspace.model.Teacher;

import java.util.List;

public interface TeacherWorkspaceService {

    List<Teacher> getAllTeacher();

    Teacher getTeacherInfo(Long teacher_id);

    ClassBasicInfo getClassBasicInfo(Long teacher_id);

    List<String> getTeacherByIdentity();

    List<LessonTeacher> getTeacherByClass(int gradeId, int classId);

    LessonTeacher getMonitorByClass(int gradeId, int classId);
}
