package com.fiveup.core.teacherworkspace.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shilin
 * @date 2022/9/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    private Long userId;
    private String realName;
    private Integer gender;
    private String contactInfo;
    private Integer identity;
    private String username;
    private String password;
}