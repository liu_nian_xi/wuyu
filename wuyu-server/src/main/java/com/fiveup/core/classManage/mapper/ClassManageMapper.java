package com.fiveup.core.classManage.mapper;

import com.fiveup.core.classManage.model.CTCorrelation;
import com.fiveup.core.classManage.model.ClassAndMonitor;
import com.fiveup.core.classManage.model.ClassInfo;
import com.fiveup.core.classManage.model.StudentInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ClassManageMapper {

    //
    @Select("select grade_id, class_id, teacher_id, gender, real_name, contact_info, identity " +
            "from correlation_grade_teacher a inner join basic_user b on a.teacher_id = b.user_id where identity=3")
    List<ClassAndMonitor> getAllClassInfo();

    @Insert("insert into correlation_grade_teacher(grade_id, class_id, teacher_id) values(#{gradeId}, #{classId}, #{teacherId})")
    int addTeacherToClass(CTCorrelation ctCorrelation);

    @Insert("insert into basic_grade(id, grade_id, class_id, class_name) values(#{id}, #{gradeId}, #{classId}, #{className})")
    int addClass(ClassInfo classinfo);

    @Select("select student_id, student_name, gender from basic_student where grade_id=#{gradeId} and class_id=#{classId}")
    List<StudentInfo> getStudentsByClass(int gradeId, int classId);

    @Select("select class_id from basic_grade where grade_id=#{gradeId}")
    List<Integer> getAllClassByGrade(int gradeId);
}
