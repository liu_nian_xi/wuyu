package com.fiveup.core.classManage.controller;

import com.fiveup.core.classManage.model.CTCorrelation;
import com.fiveup.core.classManage.model.ClassAndMonitor;
import com.fiveup.core.classManage.model.ClassInfo;
import com.fiveup.core.classManage.model.StudentInfo;
import com.fiveup.core.classManage.service.ClassManageService;
import com.fiveup.core.management.common.CommonResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/class")
public class ClassManageController {

    @Resource
    private ClassManageService classManageService;

    @GetMapping("/getClassByGradeId")
    public CommonResponse getClassByGradeId(int gradeId) {
        List<Integer> classList;
        classList = classManageService.getAllClassByGrade(gradeId);
        return CommonResponse.ok(classList);
    }

    @GetMapping(value="/getAllClassInfo")
    public CommonResponse getAllClassInfo() {
        List<ClassAndMonitor> classAndMonitor;
        classAndMonitor = classManageService.getAllClassInfo();
        return CommonResponse.ok(classAndMonitor);
    }

    @PostMapping(value="/addClass")
    public CommonResponse addClass(@RequestBody ClassInfo classInfo) {
        int code = classManageService.addClass(classInfo);
        return CommonResponse.ok(code);
    }

    @PostMapping(value="/addTeacherToClass")
    public CommonResponse addTeacherToClass(@RequestBody Map<String, List<CTCorrelation>> correlationList) {
        List<CTCorrelation> correlations = correlationList.get("correlationList");
        for(CTCorrelation ctCorrelation: correlations) {
            classManageService.addTeacherToClass(ctCorrelation);
        }
        return CommonResponse.ok();
    }

    @GetMapping(value="/getStudentsByClass")
    public CommonResponse getStudentsByClass(int gradeId, int classId) {
        List<StudentInfo> studentList;
        studentList = classManageService.getStudentsByClass(gradeId, classId);
        return CommonResponse.ok(studentList);
    }
}
