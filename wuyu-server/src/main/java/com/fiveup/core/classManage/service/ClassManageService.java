package com.fiveup.core.classManage.service;


import com.fiveup.core.classManage.model.CTCorrelation;
import com.fiveup.core.classManage.model.ClassAndMonitor;
import com.fiveup.core.classManage.model.ClassInfo;
import com.fiveup.core.classManage.model.StudentInfo;

import java.util.List;

public interface ClassManageService {

    List<ClassAndMonitor> getAllClassInfo();

    int addTeacherToClass(CTCorrelation ctCorrelation);

    int addClass(ClassInfo classinfo);

    List<StudentInfo> getStudentsByClass(int gradeId, int classId);

    List<Integer> getAllClassByGrade(int gradeId);
}
