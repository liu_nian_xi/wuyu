package com.fiveup.core.classManage.service.impl;


import com.fiveup.core.classManage.mapper.ClassManageMapper;
import com.fiveup.core.classManage.model.CTCorrelation;
import com.fiveup.core.classManage.model.ClassAndMonitor;
import com.fiveup.core.classManage.model.ClassInfo;
import com.fiveup.core.classManage.model.StudentInfo;
import com.fiveup.core.classManage.service.ClassManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: ParentServiceImpl
 * @Author: shilin
 * @Date: 2022/9/18 16:32
 */
@Service
public class ClassManageServiceImpl implements ClassManageService {

    @Autowired
    private ClassManageMapper classManageMapper;

    @Override
    public List<ClassAndMonitor> getAllClassInfo() {
        List<ClassAndMonitor> classAndMonitor;
        classAndMonitor = classManageMapper.getAllClassInfo();
        return classAndMonitor;
    }

    @Override
    public int addTeacherToClass(CTCorrelation ctCorrelation) {
        return classManageMapper.addTeacherToClass(ctCorrelation);
    }

    @Override
    public int addClass(ClassInfo classInfo) {
        int code = classManageMapper.addClass(classInfo);
        return code;
    }

    @Override
    public List<StudentInfo> getStudentsByClass(int gradeId, int classId) {
        List<StudentInfo> studentList;
        studentList = classManageMapper.getStudentsByClass(gradeId, classId);
        return studentList;
    }

    @Override
    public List<Integer> getAllClassByGrade(int gradeId) {
        List<Integer> classList;
        classList = classManageMapper.getAllClassByGrade(gradeId);
        return classList;
    }
}
