package com.fiveup.core.commentgeneration.controller;

import com.alibaba.fastjson.JSON;
import com.fiveup.core.commentgeneration.bean.Corpus;
import com.fiveup.core.commentgeneration.service.CorpusService;
import com.fiveup.core.commentgeneration.utils.Result;
import com.fiveup.core.commentgeneration.vo.CorpusVO;
import org.python.modules._sre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 处理与语料库相关的请求
 */
@CrossOrigin
@Controller
@RequestMapping("/corpus")
public class CorpusController {

    @Autowired
    private CorpusService corpusService;

    /**
     * 获取所有语料库信息
     * @return
     */
    @ResponseBody
    @GetMapping("/all")
    public String getAll() {
        Result result = new Result();

        // 查询所有语料库数据
        List<CorpusVO> all = corpusService.getAll();
        result.setCode(600);
        result.setData(all);
        result.setCount(all.size());
        result.setMsg("查询成功！");
        return JSON.toJSON(result).toString();
    }

    /**
     * 增加数据
     * @param corpus
     * @return
     */
    @ResponseBody
    @PostMapping("/add")
    public String add(@RequestBody Corpus corpus){
        Result result = new Result();

        // 增加
        corpusService.add(corpus);
        result.setCode(600);
        result.setMsg("增加成功！");
        return JSON.toJSON(result).toString();
    }

    /**
     * 删除数据
     * @param corpus
     * @return
     */
    @ResponseBody
    @DeleteMapping("/delete")
    public String delete(@RequestBody Corpus corpus){
        Result result = new Result();

        // 删除
        corpusService.deleteById(corpus.getId());
        result.setCode(600);
        result.setMsg("删除成功！");
        return JSON.toJSON(result).toString();
    }

    /**
     * 删除数据
     * @param corpus
     * @return
     */
    @ResponseBody
    @PutMapping("/update")
    public String update(@RequestBody Corpus corpus){
        Result result = new Result();

        // 更新
        corpusService.updateById(corpus);
        result.setCode(600);
        result.setMsg("更新成功！");
        return JSON.toJSON(result).toString();
    }

    @ResponseBody
    @DeleteMapping("/deleteAll")
    public String deleteAll(Integer[] ids){
        Result result = new Result();
        if(corpusService.deleteAll(ids) == ids.length){
            result.setCode(600);
            result.setMsg("删除成功！");
        }else{
            result.setCode(600);
            result.setMsg("删除失败！");
        }
        return JSON.toJSON(result).toString();
    }
}
