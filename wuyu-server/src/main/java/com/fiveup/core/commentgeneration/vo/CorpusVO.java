package com.fiveup.core.commentgeneration.vo;

import com.fiveup.core.commentgeneration.bean.Subject;

/**
 * 语料库
 */
public class CorpusVO {
    // ID
    private Integer id;
    // 科目
    private Subject subject;
    // 分数（以此分数为基准）
    private Integer score;
    // 对应的评价词
    private String comment;

    public CorpusVO() {
    }

    public CorpusVO(Integer id, Subject subject, Integer score, String comment) {
        this.id = id;
        this.subject = subject;
        this.score = score;
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
