package com.fiveup.core.commentgeneration.bean;

/**
 * 语料库
 */
public class Corpus {
    // ID
    private Integer id;
    // 科目ID
    private Integer subjectId;
    // 分数（以此分数为基准）
    private Integer score;
    // 对应的评价词
    private String comment;

    public Corpus() {
    }

    public Corpus(Integer id, Integer subjectId, Integer score, String comment) {
        this.id = id;
        this.subjectId = subjectId;
        this.score = score;
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
