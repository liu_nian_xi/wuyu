package com.fiveup.core.commentgeneration.mapper;

import com.fiveup.core.commentgeneration.bean.Corpus;
import com.fiveup.core.commentgeneration.vo.CorpusVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 对语料库的数据操作
 */
public interface CorpusMapper {
    /**
     * 插入数据
     * @param corpus
     */
    void insert(Corpus corpus);

    /**
     * 根据ID删除数据
     * @param id
     */
    void deleteById(Integer id);

    /**
     * 根据ID更新数据
     * @param corpus
     */
    void updateById(Corpus corpus);
    /**
     * 查询所有数据
     * @return
     */
    List<Corpus> selectList();

    /**
     * 查询所有已封装数据
     * @return
     */
    List<CorpusVO> selectAll();

    /**
     * 根据ID查询数据
     * @param id
     * @return
     */
    CorpusVO selectById(Integer id);

    Integer deleteAll(@Param("ids") Integer[] ids);
}
