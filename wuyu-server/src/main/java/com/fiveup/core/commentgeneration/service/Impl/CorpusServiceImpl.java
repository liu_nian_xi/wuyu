package com.fiveup.core.commentgeneration.service.Impl;

import com.fiveup.core.commentgeneration.bean.Corpus;
import com.fiveup.core.commentgeneration.mapper.CorpusMapper;
import com.fiveup.core.commentgeneration.service.CorpusService;
import com.fiveup.core.commentgeneration.vo.CorpusVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 语料及评论业务逻辑实现
 */
@Service
public class CorpusServiceImpl implements CorpusService {

    @Autowired
    private CorpusMapper corpusMapper;

    @Override
    public void add(Corpus corpus) {
        corpusMapper.insert(corpus);
    }

    @Override
    public void deleteById(Integer id) {
        corpusMapper.deleteById(id);
    }

    @Override
    public void updateById(Corpus corpus) {
        corpusMapper.updateById(corpus);
    }

    @Override
    public List<Corpus> getList() {
        List<Corpus> corpuses = corpusMapper.selectList();
        return corpuses;
    }

    @Override
    public List<CorpusVO> getAll() {
        List<CorpusVO> corpusVOS = corpusMapper.selectAll();
        return corpusVOS;
    }

    @Override
    public CorpusVO getById(Integer id) {
        CorpusVO corpusVO = corpusMapper.selectById(id);
        return corpusVO;
    }

    @Override
    public Integer deleteAll(Integer[] ids) {
        return corpusMapper.deleteAll(ids);
    }
}
