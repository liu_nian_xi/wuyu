package com.fiveup.core.commentgeneration.mapper;

import com.fiveup.core.commentgeneration.bean.Subject;
import com.fiveup.core.performanceevaluation.vo.SubjectScoreWeightVO;
import jnr.ffi.annotations.In;

import java.util.List;

/**
 * 对科目的数据操作
 */
public interface SubjectMapper {
    /**
     * 查询所有数据
     * @return
     */
    List<Subject> selectAll();

    /**
     * 根据ID查询数据
     * @param id
     * @return
     */
    Subject selectById(Integer id);
}
