package com.fiveup.core.diagnose.controller;

import com.fiveup.core.diagnose.service.FiveupManagementService;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.text.ParseException;

@Controller
@RequestMapping("/diagnose")
public class FiveupManagement {

    @Autowired
    FiveupManagementService fiveupManagementService;

    @RequestMapping("/insertstudentscore")
    @ResponseBody
    public String Insertstudentscore(@RequestParam("id") int id,@RequestParam("deyu") int deyu,
    @RequestParam("zhiyu") int zhiyu,@RequestParam("tiyu") int tiyu,
                                     @RequestParam("meiyu") int meiyu,@RequestParam("laoyu") int laoyu,
                                     @RequestParam("sdate") String sdate
    ) throws ParseException {
        fiveupManagementService.insertstudentscore(id,deyu,zhiyu,tiyu,meiyu,laoyu,sdate);
        return "success";
    }

    @RequestMapping("/updatestudentscore")
    @ResponseBody
    public String Updatestudentscore(@RequestParam("id") int id,@RequestParam("deyu") int deyu,
                                     @RequestParam("zhiyu") int zhiyu,@RequestParam("tiyu") int tiyu,
                                     @RequestParam("meiyu") int meiyu,@RequestParam("laoyu") int laoyu
    ) throws ParseException {
        fiveupManagementService.updatestudentscore(id,deyu,zhiyu,tiyu,meiyu,laoyu);
        return "success";
    }

}
