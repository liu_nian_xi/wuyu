package com.fiveup.core.diagnose.service;


import com.fiveup.core.diagnose.mapper.SPcommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SPcommentService {

    @Autowired
    SPcommentMapper sPcommentMapper;
    public void doComment(Long id,String comment){
        sPcommentMapper.InsertComment(id,comment);
    }
}
