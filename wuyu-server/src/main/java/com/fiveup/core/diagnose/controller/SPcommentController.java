package com.fiveup.core.diagnose.controller;


import com.fiveup.core.diagnose.service.SPcommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/diagnose")
public class SPcommentController {

    @Autowired
    SPcommentService sPcommentService;
    @GetMapping("/SPcomment")
    @ResponseBody
    public String doCommnet(@RequestParam("id") Long id, @RequestParam("comment") String comment){
        sPcommentService.doComment(id,comment);
        return "success";
    }
}
