package com.fiveup.core.diagnose.mapper;


import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SPcommentMapper {

    /*新增或更新评价*/
    @Insert("replace into di_studentplancomment(sp_commentid,sp_comment) values(#{id},#{comment})")
    public void InsertComment(Long id,String comment);

}
