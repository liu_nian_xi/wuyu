package com.fiveup.core.diagnose.controller;

import com.fiveup.core.diagnose.bean.Student_plan;
import com.fiveup.core.diagnose.service.SplanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/diagnose")
public class SplanController {

    @Autowired
    SplanService splanService;

    @GetMapping("/getallplan")
    @ResponseBody
    public List<Student_plan> getallPlan() {

        return splanService.getallPlan();

    }
    /*通过id获取计划信息*/
    @GetMapping("/getplanbyid")
    @ResponseBody
    public List<Student_plan> getPlanByid(@RequestParam("name") String name, @RequestParam("id") Long id) {

        return splanService.getPlanByid(name,id);
    }
}
