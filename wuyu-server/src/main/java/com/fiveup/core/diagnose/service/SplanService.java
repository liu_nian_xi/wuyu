package com.fiveup.core.diagnose.service;


import com.fiveup.core.diagnose.bean.Student_plan;
import com.fiveup.core.diagnose.mapper.SplanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SplanService {

    @Autowired
    SplanMapper splanMapper;

    public List<Student_plan> getallPlan(){
        return splanMapper.getallPlan();
    }
    public List<Student_plan> getPlanByid(String name,Long id){
        return splanMapper.getPlanByid(name,id);
    }
}
