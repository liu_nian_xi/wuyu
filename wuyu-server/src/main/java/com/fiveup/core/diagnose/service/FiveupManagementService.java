package com.fiveup.core.diagnose.service;


import com.fiveup.core.diagnose.controller.FiveupManagement;
import com.fiveup.core.diagnose.mapper.FiveupManagementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
public class FiveupManagementService {
    @Autowired
    FiveupManagementMapper fiveupManagementMapper;
    public void insertstudentscore(int id, int deyu, int zhiyu, int tiyu, int meiyu, int laoyu, String sdate) throws ParseException {
        sdate = sdate.replace("Z", " UTC");
        SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        SimpleDateFormat defaultFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date1 = utcFormat.parse(sdate);
        fiveupManagementMapper.insertstudentscore(id,deyu,zhiyu,tiyu,meiyu,laoyu,date1);

    };

    public void updatestudentscore(int id, int deyu, int zhiyu, int tiyu, int meiyu, int laoyu){

        fiveupManagementMapper.updatestudentscore(id,deyu,zhiyu,tiyu,meiyu,laoyu);

    };
}
