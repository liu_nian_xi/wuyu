package com.fiveup.core.remark.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fiveup.core.remark.common.Result;
import com.fiveup.core.remark.entity.student;
import com.fiveup.core.remark.mapper.re_studentMapper;
import com.fiveup.core.remark.service.createremarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/student")
//@RequestMapping("/api/student")
public class re_studentController {

    @Autowired
    re_studentMapper studentMapper;

    @PostMapping
    public Result<?> save(@RequestBody student student){
        studentMapper.insert(student);
        return Result.success();
    }
    @GetMapping("/save_get")
    public Result<?> save_get(@RequestParam("sid") Integer Sid,
                           @RequestParam("name") String Name,
                           @RequestParam("virtue") Integer Virtue,
                           @RequestParam("intelligence") Integer Intelligence,
                           @RequestParam("sports") Integer Sports,
                           @RequestParam("art") Integer Art,
                           @RequestParam("labor") Integer Labor,
                           @RequestParam(value = "remark",defaultValue = "") String Remark,
                           @RequestParam("tid") Integer Tid){
        student student = new student();
        double v_ave = 0;
        double i_ave = 0;
        double s_ave = 0;
        double a_ave = 0;
        double l_ave = 0;
        student.setSid(Sid);
        student.setName(Name);
        student.setVirtue(Virtue);
        student.setIntelligence(Intelligence);
        student.setSports(Sports);
        student.setArt(Art);
        student.setLabor(Labor);
        student.setTid(Tid);
//        student.setRemark(Remark);
        //       单独生成评语
        HashMap<String,Object> map = new HashMap<>();
        map.put("tid",Tid);
        List<student> students = studentMapper.selectByMap(map);
        for (com.fiveup.core.remark.entity.student value : students) {
            v_ave += value.getVirtue();
            i_ave += value.getIntelligence();
            s_ave += value.getSports();
            a_ave += value.getArt();
            l_ave += value.getLabor();
        }
        v_ave = v_ave/students.size();
        i_ave = i_ave/students.size();
        s_ave = s_ave/students.size();
        a_ave = a_ave/students.size();
        l_ave = l_ave/students.size();
        Remark = createremarkService.createremark(v_ave,i_ave,s_ave,a_ave,l_ave,student);
        student.setRemark(Remark);
//
        System.out.println("开始新增学生，学生信息为：");
        System.out.println(student);
        studentMapper.insert(student);
        return Result.success();
    }
    @GetMapping("/edit_get")
    public Result<?> edit_get(@RequestParam("sid") Integer Sid,
                           @RequestParam("id") Integer Id,
                           @RequestParam("name") String Name,
                           @RequestParam("virtue") Integer Virtue,
                           @RequestParam("intelligence") Integer Intelligence,
                           @RequestParam("sports") Integer Sports,
                           @RequestParam("art") Integer Art,
                           @RequestParam("labor") Integer Labor,
                           @RequestParam("remark") String Remark,
                           @RequestParam("tid") Integer Tid){
        student student = new student();
        student.setId(Id);
        student.setSid(Sid);
        student.setName(Name);
        student.setVirtue(Virtue);
        student.setIntelligence(Intelligence);
        student.setSports(Sports);
        student.setArt(Art);
        student.setLabor(Labor);
        student.setRemark(Remark);
        student.setTid(Tid);
//        student.setTid(Tid);
        System.out.println("前端传来的修改后的学生信息是：");
        System.out.println(student);
        studentMapper.updateById(student);
        return Result.success();
    }
    @PutMapping
    public Result<?> update(@RequestBody student student){
        studentMapper.updateById(student);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> update(@PathVariable Long id){
        studentMapper.deleteById(id);
        return Result.success();
    }

    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize,
                              @RequestParam(defaultValue = "") String search){
        LambdaQueryWrapper<student> wrapper= Wrappers.<student>lambdaQuery();
        if(StrUtil.isNotBlank(search)){
            wrapper.like(student::getName,search);
        }
        Page<student> studentPage = studentMapper.selectPage(new Page<>(pageNum,pageSize), wrapper);
        return Result.success(studentPage);
    }
    @GetMapping("/boy")
    public student findmsg(@RequestParam("id") Integer Sid){
//        student student = studentMapper.selectById(Id);
//        增加sid
        student student = studentMapper.findstumsg(Sid);
        return student;
    }
    @GetMapping("/average")
    public HashMap<String, Object> findsum(@RequestParam("tid") Integer Tid){
        HashMap<String,Object> map = new HashMap<>();
        HashMap<String,Object> map1 = new HashMap<>();
        double v_sum = 0;
        double i_sum = 0;
        double s_sum = 0;
        double a_sum = 0;
        double l_sum = 0;
        map.put("tid",Tid);
        List<student> student = studentMapper.selectByMap(map);
//        System.out.println(123);
//        student.forEach(System.out::println);
//        System.out.println(student.size());
        for (com.fiveup.core.remark.entity.student value : student) {
            v_sum += value.getVirtue();
            i_sum += value.getIntelligence();
            s_sum += value.getSports();
            a_sum += value.getArt();
            l_sum += value.getLabor();
        }
        v_sum = v_sum/student.size();
        i_sum = i_sum/student.size();
        s_sum = s_sum/student.size();
        a_sum = a_sum/student.size();
        l_sum = l_sum/student.size();
        map1.put("v_sum",v_sum);
        map1.put("i_sum",i_sum);
        map1.put("s_sum",s_sum);
        map1.put("a_sum",a_sum);
        map1.put("l_sum",l_sum);
        return map1;
    }
    @GetMapping("/sum")
    public HashMap<Integer, Object> findall(@RequestParam("tid") Integer Tid){
        HashMap<String,Object> map = new HashMap<>();
        HashMap<Integer,Object> map1 = new HashMap<>();
        int sum = 0;
        int flag = 1;
        map.put("tid",Tid);
        List<student> student = studentMapper.selectByMap(map);
        for (com.fiveup.core.remark.entity.student value : student){
            sum = sum + value.getVirtue()+value.getIntelligence()+value.getSports()+value.getArt()+ value.getLabor();
            map1.put(flag,sum);
            flag += 1;
            sum = 0;
        }
//        System.out.println("size of map1");
//        System.out.println(map1.size());
        return map1;
    }
    @GetMapping("/remark")
    public student findremark(@RequestParam("id") Integer Id,@RequestParam("tid") Integer Tid){
//        获得当前学生信息
//        增加sid
//        student student = studentMapper.selectById(Id);
        student student = studentMapper.findstumsg(Id);
        HashMap<String,Object> map = new HashMap<>();
        String remark = "";
        double v_ave = 0;
        double i_ave = 0;
        double s_ave = 0;
        double a_ave = 0;
        double l_ave = 0;
//      获得当前班级各科平均分
        map.put("tid",Tid);
        List<student> students = studentMapper.selectByMap(map);
        for (com.fiveup.core.remark.entity.student value : students) {
            v_ave += value.getVirtue();
            i_ave += value.getIntelligence();
            s_ave += value.getSports();
            a_ave += value.getArt();
            l_ave += value.getLabor();
        }
        v_ave = v_ave/students.size();
        i_ave = i_ave/students.size();
        s_ave = s_ave/students.size();
        a_ave = a_ave/students.size();
        l_ave = l_ave/students.size();
//      开始生成成绩评价
//        remark = createremarkService.createremark(v_ave,i_ave,s_ave,a_ave,l_ave,student);
//        student.setRemark(remark);
//        studentMapper.updateById(student);
        return student;
    }
    @GetMapping("/allremark")
    public void allremark(@RequestParam("tid") Integer Tid){
        HashMap<String,Object> map = new HashMap<>();
        String remark = "";
        double v_ave = 0;
        double i_ave = 0;
        double s_ave = 0;
        double a_ave = 0;
        double l_ave = 0;
        map.put("tid",Tid);
        List<student> students = studentMapper.selectByMap(map);
        for (com.fiveup.core.remark.entity.student value : students) {
            v_ave += value.getVirtue();
            i_ave += value.getIntelligence();
            s_ave += value.getSports();
            a_ave += value.getArt();
            l_ave += value.getLabor();
        }
        v_ave = v_ave/students.size();
        i_ave = i_ave/students.size();
        s_ave = s_ave/students.size();
        a_ave = a_ave/students.size();
        l_ave = l_ave/students.size();
        System.out.println("开始生成全部学生评语");
        for (com.fiveup.core.remark.entity.student value : students) {
            remark = createremarkService.createremark(v_ave,i_ave,s_ave,a_ave,l_ave,value);
            value.setRemark(remark);
            studentMapper.updateById(value);
        }
        System.out.println("生成成功！");
    }
}
