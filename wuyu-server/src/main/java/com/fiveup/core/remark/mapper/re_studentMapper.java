package com.fiveup.core.remark.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fiveup.core.remark.entity.student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface re_studentMapper extends BaseMapper<student> {
//  根据学生学号从学生表中取出所有信息
    @Select("select * FROM re_student where sid=#{Sid}")
    public student findstumsg(Integer sid);
}
